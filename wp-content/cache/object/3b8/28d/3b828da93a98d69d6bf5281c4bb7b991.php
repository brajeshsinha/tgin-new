��[<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:105070;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2018-08-29 09:46:00";s:13:"post_date_gmt";s:19:"2018-08-29 09:46:00";s:12:"post_content";s:16295:"[vc_row][vc_column el_class="sec-heading"][vc_column_text]
<h2>Operations</h2>
[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]<h1><strong>Operations Manager – Chicago</strong></h1>
<p>We’re looking for a fearless and brilliant Operations and Logistics Manager to help grow and scale operations. As the Operations Manager you will be responsible for the development and growth of our business. You are forward-thinking and flexible as we play with how to run our business. You are a pioneer in innovating our growth and an operational savant.In this role, the Operations Manager is directly and indirectly responsible for activities including overseeing production, tracking and monitoring all facets of our delivery processes, and implementing enhancements to our operating processes. The Operations Manager will collaborate across functions and departments (Warehouse, Sales, Marketing, etc.) in the execution of his or her responsibilities.</p>

<p><strong><u>What you’ll do:<br />
</u></strong></p>
<ul>
<li><strong>Develop & Optimize Processes. </strong>Scale new and existing processes to allow the business to efficiently operate and grow. We are growing quickly, and many of our processes are outdated and based on how we were operating when we first started running the company in 2014. </li>
<li><strong>Analytics. </strong>Heavy analysis of customer purchases, customer service metrics, and customer engagement to properly allocate supply, guide employee incentive programs, and ensure high-quality user experiences</li>
<ul>
<li>Forecast and adjust payroll to maximize productivity and complete task workload (e.g., shipment, planogram, inventory, and ad set processes) on time, and within the payroll budget.</li>
<li>Review and interpret financial and operational reporting regularly, including store visit and audit results.</li>
</ul>
<li><strong>	Strategy. </strong>Decide which projects the team should focus on and set an aggressive timeline to reach those goals</li>
<li><strong>	Product Improvement.  </strong>Proactively search for new features which could improve a seamless customer experience</li>
<li><strong>	VendorRelations.</strong>Identifyandattack problem areas with respect to our suppliers, and negotiate with businesses partners for reduced costs</li>
<li><strong>	Customer Support. </strong>Work with CRM software to determine how customer feedback impacts business processes</li>
<li><strong>	Hustle. </strong>Take ownership of tgin’sgrowth  and do whatever it takes to get the job done
	Marketing. Coordinate with the marketing team to assess new opportunities like launching a new market, spinning off a new product line, experimenting with new business models… the possibilities are endless!</li>
<li><strong>Human Resources. </strong>On Board and train new employees and experiment with new ways to do so with minimal physical presence. Build a team that embodies the tgin brand by delivering exceptional service through the execution of tasks that drive product availability, newness, and the option for a self-navigated shopping experience.</li>
<li>Continue to grow REVENUES!</li>
</ul>
<p><strong><u>Requirements:</u></strong></p>
<ul>
<li>3-5 years of operations management experience</li>
<li>College degree. MBA a plus. </li>
<li>Strong verbal and written communication skills </li>
<li>Data-driven decision mentality and sound business judgment through strong analytical thinking</li>
<li>Experience managing and growing companies</li>
<li>Hustler- You have no problem getting your hands dirty and doing the gritty work that comes along with the higher-level thinking</li>
<li>Creative solutions driven mindset, with a get shit done attitude</li>
<li>Excel-master</li>
<li>High Emotional IQ – Can deal with drivers, influencers and clients no problem</li>
<li>Stellar networking skills and the ability to make smart partnerships happen</li>
<li>Flexible and forward thinking- We need someone sharp and eager to play with different models to help us scale</li>
<li>Entrepreneurial DNA and fear tolerance of a honey-badger</li>
</ul>
<p><strong>COMPENSATION: Full-time salary negotiable based on experience.</strong></p>

<p>Please email resume and cover letter to <a href="http://tgincareers@gmail.com">tgincareers@gmail.com</a></p>
[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]
<h1><strong>Warehouse/Operations Manager – Chicago</strong></h1>
<strong><u>Overview</u></strong>

Join the creative and energetic <strong>Thank God It’s Natural (tgin)</strong> Operations Team, and help us further enhance our strong brand. We are a small natural hair business looking to hire a Warehouse Manager to oversee factory operations. As our Warehouse Manager you must be meticulous and extremely detailed oriented, comfortable working in a fast paced environment and capable of multi-tasking.

<strong><u>Shipping
</u></strong>
<ul>
	<li>Oversee the Shipping Department</li>
	<li>Draft and Maintain Shipping Best Practices Document</li>
	<li>Review orders shipped daily from Amazon and 3dcart customer orders.</li>
	<li>Create, package, and ship domestic/international wholesale orders.</li>
	<li>Operate the shopping cart software to retrieve and prepare orders.</li>
	<li>Schedule pickup times to ship daily orders (USPS) and wholesale orders (Fed Ex).</li>
	<li>Respond to Amazon.com customer inquiries.</li>
	<li>Order shipping supplies from USPS &amp; Staples while staying within budget.</li>
	<li>Verify incoming shipments from UPS &amp; Fed Ex, and the like.</li>
	<li>Provide services: process returns, exchanges, adjustments and payments.</li>
	<li>Track and report the status of inventory, orders, and analyze data from multiple sources to effectively problem solve on behalf of customers/retailers.</li>
	<li>File Claims for lost or stolen packages.</li>
	<li>Respond toCRMs via 3dshopping cart.</li>
	<li>Meet with representatives USPS, Fed Ex, UPS to negotiate lower rates.</li>
</ul>
<strong><u>Production</u></strong>
<ul>
	<li>Review production schedule every Monday and forecast inventory based on schedule.</li>
	<li>Bottle/label/make conditioners, honey miracle, shea butter, argan oils, sample packs, etc. products on an as needed basis.</li>
	<li>Oversee daily clean of the production area and office to ensure organization and order.</li>
	<li>Understand the shelf life of tgin’s natural hair and skin care products.</li>
	<li>Stock shelves using the FIFO method.</li>
</ul>
<strong><u>Operations
</u></strong>
<ul>
	<li>Decide which projects the Production/Shipping team should focus on and set an aggressive timeline to reach those goals.</li>
	<li>Update inventory data in Google Docs on weekly basis.</li>
	<li>Identify, recommend and implement changes to improve productivity and reduce cost and scrap, monitor scrap and rework data.</li>
	<li>Direct the establishment, implementation and maintenance of production standards.</li>
	<li>Direct and coordinate various programs essential to manufacturing procedures (e.g., training, safety, housekeeping, cost reduction, worker involvement, security, etc.).</li>
	<li>Initiate and coordinate major projects, (e.g., plant layout changes, installation of capital equipment, major repairs, etc.).</li>
	<li>Develop the manufacturing plan and establish procedures for maintaining high standards of manufacturing operations to ensure that products conform to established customer and company quality standards.</li>
	<li>Perform miscellaneous duties and projects as assigned and required.</li>
</ul>
<strong>COMPENSATION: </strong>Full-time salary negotiable based on experience.

Please email resume and cover letter to <a href="http://tgincareers@gmail.com">tgincareers@gmail.com</a>

[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]
<h1>Assistant Warehouse Manager - Chicago</h1>
<strong><u>Overview</u></strong>

Join the creative and energetic <strong>Thank God It’s Natural (tgin)</strong> Operations Team, and help us further enhance our strong brand. We are a small natural hair business looking to hire a Warehouse Assistant Manager to oversee factory operations. As our Assistant Manager you must be meticulous and extremely detailed oriented, comfortable working in a fast paced environment and capable of multi-tasking. You will report to the Warehouse manager and help to oversee warehouse processes to ensure smooth flow. You will be responsible for the warehouse when the Warehouse manager is out.

<strong><u>Responsibilities</u></strong>
<ul>
	<li>Adhere to the production schedule and directives.</li>
	<li>Communicate with upper management to meet daily production goals. Your days will involve helping to mix, pack, and label our products to including but not limited to oil and shea butter.</li>
	<li>Assist with keeping the warehouse stocked with materials, break down boxes for recycling, and clean and organize the warehouse at the end of the day.</li>
	<li>Cross-training will also be required in our Shipping Department, as you will back them up in times of heavy shipping, such as: holidays, sales, and events, and any special projects that may arise.</li>
	<li>Shipping tasks include lifting, organizing, assembling and labeling boxes, case-packing product and getting it on the shelf for our wholesale customers.</li>
	<li>Must be comfortable hauling in large material shipments on pallet jacks.</li>
	<li>Oversee each department’s daily clean up at the end of each production day to maintain a safe and clean working environment. You will also be responsible for weekly cleanup of our communal kitchen and production areas.</li>
	<li>Special projects as needed.</li>
</ul>
<strong><u>Abilities</u></strong>
<ul>
	<li>The ability to shift gears quickly.</li>
	<li>Maintains focus, skill, and speed to help the team maintain output goals.</li>
	<li>You must be self-motivated, careful, attentive, and on task at all times in production.</li>
	<li>Must have a strong work ethic.</li>
	<li>Candidate would ideally be familiar with production and shipping duties.</li>
	<li>You must be able to lift 50 pounds repeatedly.</li>
	<li>The ability to stand on your feet between 6 to 7 hours per day.</li>
	<li>As you will be working hands-on with the materials you must be able to tolerate direct contact with fragrance and essential oils.</li>
</ul>
* Interested applicants should send cover letters and resumes to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a> for consideration.

[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]
<h1><strong>tgin Administrative Assistant - Operations</strong></h1>
<strong><u>Company Overview</u></strong>

Join the creative and energetic <strong>Thank God It’s Natural (tgin)</strong> Operations Team, and help us further enhance our strong brand. We are a small natural hair business looking to hire an Administrative Assistant to assist our Operations Team, which is primarily responsible for ordering the necessary components of our products and shipping and delivering orders to major retailers. As our Administrative Assistant, you must be meticulous and extremely detailed oriented, comfortable working in a fast pace environment and capable multi-tasking. Experience with Facebook Ads or social media is a major bonus/plus.

<strong><u>Responsibilities</u></strong>
<ul>
	<li>Manage multiple e-mail accounts (admin account and CEO’s account)</li>
	<li>Calendar/Schedule Management for CEO</li>
	<li>Order supplies for production runs</li>
	<li>Schedule deliveries for retailers using our logistics system</li>
	<li>Update account payables</li>
	<li>Send CEO an email of all major emails at the end of the day</li>
	<li>Coordinate and plan events according to event checklist</li>
	<li>Return Calls and Voicemails, Reminder Calls/Texts</li>
	<li>Travel Planning (Manage Travel Preferences for Air, Hotel, Car, Vacations, Private Charter Flights, &amp; etc)</li>
	<li>Event Planning, Prep, Coordination, &amp; Meal Planning</li>
	<li>Notetaking on conference calls</li>
	<li>Social Media Administration (Blogs/Postings, Facebook, Twitter, LinkedIn, YouTube, etc.) – We have a social media team, but there may be some minor posts that you are required to put up occasionally.</li>
	<li>Editing &amp; Proofreading</li>
	<li>On-line File Management</li>
</ul>
<strong><u>Education and Experience</u></strong>
<ul>
	<li>Strong Written and Oral Communication Skills</li>
	<li>Extremely Organized</li>
	<li>Acute multi-tasking skills</li>
	<li>Flexible and able to Work in a Fast Paced Environment</li>
	<li>Exceptional Time Management Skills</li>
	<li>Analytical</li>
</ul>
<strong>COMPENSATION: </strong>TBD, but negotiable based on experience.

Please email resume,  cover letter and salary requirements to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>

[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]
<h1><strong>Shipping and Receiving Coordinator — Chicago Only</strong></h1>
We are looking for individual who is a self-starter and has strong computer skills to head up our shipping and receiving department.

<strong><u>Responsibilities</u></strong>
<ul>
	<li>Hours –  9 – 5 CST</li>
	<li>Must have strong computer skills and be able to use Word, Excel, Power Point, email and social media</li>
	<li>Operate the shopping cart software to retrieve and prepare orders</li>
	<li>Package and ship daily orders via USPS</li>
	<li>Order shipping supplies from USPS &amp; Uline while staying within a budget</li>
	<li>Verify incoming shipments from UPS &amp; Fed Ex, and other various shipping companies</li>
	<li>Communicate company policies and functions of products to customers</li>
	<li>Provide services; process returns, exchanges, adjustments and payments</li>
	<li>Train new employees on tgin shipping procedures</li>
	<li>Create, package, and ship domestic/international wholesale orders</li>
	<li>Schedule pickup times to ship daily orders (USPS) and wholesale orders (UPS)</li>
	<li>Maintain/count warehouse inventory on a daily/monthly basis respectively</li>
	<li>Alert Production Manager when stock is low for any product</li>
	<li>Available to help bottle/label/make products whenever needed</li>
	<li>Perform a daily clean of the shipping area and office to ensure organization and order</li>
	<li>Understand the shelf life of tgin’s natural body and skin care products.</li>
	<li>Assist Production Team with bottling and labeling on as needed basis</li>
	<li>Assist with general office duties (e.g. contacting the management office about building and maintenance issues, ordering supplies, taking inventory, etc.)</li>
	<li><strong>Must be able to pass a background check</strong></li>
</ul>
Interested candidates should email their resume and cover letter to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a> Subject: Shipping Department

[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]
<h1><strong>Delivery Driver — Chicago Only</strong></h1>
tgin is currently seeking to hire a delivery driver who can work 2 days a week this summer in the mornings (7am – 1pm). Driver will be responsible for dropping off product at various Mariano’s stores throughout the city and suburb and completing an inventory report.

<strong><u>Requirements</u></strong>
<ul>
	<li>Extreme attention to detail</li>
	<li>Customer-friendly personality</li>
	<li>Strong computer skills</li>
	<li>Your own lap top</li>
	<li>The ability to lift in excess of 20 lbs</li>
</ul>
Qualified applicants must have a valid driver’s license issued in the state of Illinois, their own vehicle and proof of insurance. Delivery Drivers are expected to comply with tgin  appearance guidelines and wear the company-provided uniform.  Applicants must be able to pass a criminal background check

Please email resume, cover letter and three references to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>

Pay: TBD + Mileage Reimbursement

[/vc_column_text][/vc_column][/vc_row]";s:10:"post_title";s:10:"Operations";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:10:"operations";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-09-05 11:05:42";s:17:"post_modified_gmt";s:19:"2018-09-05 11:05:42";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:52:"http://import.thankgoditsnatural.com/?page_id=105070";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}