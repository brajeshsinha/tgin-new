��[<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:105074;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2018-08-29 09:47:29";s:13:"post_date_gmt";s:19:"2018-08-29 09:47:29";s:12:"post_content";s:8062:"[vc_row type="in_container" full_screen_row_position="middle" scene_position="center" text_color="dark" text_align="left" overlay_strength="0.3"][vc_column column_padding="no-extra-padding" column_padding_position="all" background_color_opacity="1" background_hover_color_opacity="1" el_class="sec-heading" tablet_text_alignment="default" phone_text_alignment="default"][vc_column_text]
<h2>Internships</h2>
[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]
<h1><strong><i>You must send over your transcript with your application in order to be considered for any of the following positions.</i></strong></h1>
<h1><strong>Events Intern – Chicago Only</strong></h1>
<strong>Fall 2018/Summer 2019</strong>Thank God It’s Natural (tgin), a manufacturer of natural hair and body products, is seeking interns to become a part of our vibrant Marketing division, working on events, and specifically our upcoming Breast Cancer Cocktails for a Cure scheduled to take place in October. We are seeking sharp, enthusiastic interns who are on top of their game, and looking to learn more about planning and managing events! This internship is ideal for college seniors and recent college graduates, who are creative and passionate about marketing, and specifically events. Candidates must live in Chicago and come into the office!

<strong>Required Hours:</strong> 2 days a week (during business hours)You’ll be learning and applying basic sales, marketing, client relations, customer satisfaction and branding techniques.

<strong><u>Responsibilities</u></strong>

– Research potential events for tgin to support or attend

- Provide social event coverage during event

- Draft event recaps

- Assist with planning, execution and event follow up.

- Collaborate with creative design teams to drive impressions, word of mouth buzz, social media follows and bottom- line sales

- Review brand ambassador submissions

<strong><u>Knowledge, Skills &amp; Abilities/Credentials</u></strong>– Creative and/or passionate about consumer marketing

– Working knowledge of PhotoShop, Illustrator, InDesign, PowerPoint– a plus!

– Amazing social media and copy writing skills are a plus!

– Ability to work well under pressure and meet project deadlines

– Strong preference for individuals with (or pursuing) a BS/BA in Graphic Design, Marketing, Art, Photography or a related field

<strong><u>Key Personality Traits</u></strong>– Exceptional communication skills

– Exceptional multitasking

– Passion for customer service and complete satisfaction

– Ability to work both independently and remotely

– Ability to take direction and complete assignments on deadline

*You don’t need to be a student, but can get credit if you are**

* Interested applicants should send cover letters and resumes and a copy of their transcript to tgincareers@gmail.com for consideration.
<h1><strong>Marketing Intern – Creative Projects</strong></h1>
Fall 2018/Summer 2019

Thank God It’s Natural (tgin), a manufacturer of natural hair and body products, is seeking interns to become a part of our vibrant Marketing division, working on projects focused in the hair, beauty and fashion industries. We are seeking sharp, enthusiastic interns with a desire to work on creative projects involving graphic design, motion graphics, video and photography in the beauty industry! This internship is ideal for college seniors and recent college graduates, who are creative and passionate about marketing. A strong preference will be given to candidate was an art background or are graduating and or pursuing a degree in Graphic Design, Marketing, Art, Photography or a related field. Candidates must live in Chicago and come into the office!

<strong>Required Hours:</strong> 2 days a week (during business hours)

<strong><u>Responsibilities</u></strong>

– Collaborate with creative design teams to drive impressions, word of mouth buzz, social media follows and bottom- line sales

– Design content for multimedia campaigns including collateral materials, video promotions, web promotions, social media marketing and email communications

– Produce creative for event marketing initiatives including (but not limited to) press events, consumer engagement events, product launches, multi-city tours, etc.

– Assist with creative design for new business presentations and campaign reports

Examples of the Projects We’re Currently Working On:

– PR/Press Releases

– Website Sliders

– Lifestyle Photos

– Commercial Photo Shoots

– Motion Graphics

– Internal Newsletters

– Billboards

– Campaigns (wash n go, fall season, holiday, twitter chats) – See attached photo

<strong><u>Knowledge, Skills &amp; Abilities/Credentials</u></strong>– Creative and/or passionate about consumer marketing

– Working knowledge of PhotoShop, Illustrator, InDesign, PowerPoint– a must!

– Amazing social media and copy writing skills are a plus!

– Minimum 1-2 years of experience in related fields is ideal!

– Ability to work well under pressure and meet project deadlines

– Strong preference for individuals with (or pursuing) a BS/BA in Graphic Design, Marketing, Art, Photograpy or a related field

<strong><u>Key Personality Traits</u></strong>– Exceptional communication skills

– Exceptional multitasking

– Passion for customer service and complete satisfaction

– Ability to work both independently and remotely

– Ability to take direction and complete assignments on deadline

*You don’t need to be a student, but can get credit if you are**

* Interested applicants should send cover letters, resume, and a copy of their transcript to tgincareers@gmail.com for consideration.
<h1><strong> Marketing Intern (Strategy/Social) – Chicago Only</strong></h1>
<h5>Fall 2018/Summer 2019</h5>
Thank God It’s Natural (tgin), a manufacturer of natural hair and body products, is seeking interns to become a part of our vibrant Marketing division, working on special projects that enable us to increase our reach and differentiate ourselves as a brand. We are seeking sharp, enthusiastic interns who are on top of their game, and looking to learn more about marketing strategy, social media, graphic design, etc.! This internship is ideal for college seniors and recent college graduates, who are creative and passionate about marketing, and specifically events. Candidates must live in Chicago and come into the office!

<strong>Required Hours:</strong> 2 days a week (during business hours)

<strong><u>Responsibilities</u></strong>

- Review existing strategies and provide recommendations for improvements

- Brainstorm ideas and develop plans of actions for increasing consumer engagement

- Assist with marketing related projects (e.g. sampling, event organization, reviewing marketing materials, etc.)

- Develop content for Social Media Team

<strong><u>Knowledge, Skills &amp; Abilities/Credentials</u></strong>– Creative and/or passionate about consumer marketing

– Working knowledge of PhotoShop, Illustrator, InDesign, PowerPoint– a plus!

– Amazing social media and copy writing skills are a plus!

– Ability to work well under pressure and meet project deadlines

– Strong preference for individuals with (or pursuing) a BS/BA in Graphic Design, Marketing, Art, Photography or a related field

<strong><u>Key Personality Traits</u></strong>– Exceptional communication skills

– Exceptional multitasking

– Passion for customer service and complete satisfaction

– Ability to work both independently and remotely

– Ability to take direction and complete assignments on deadline

*You don’t need to be a student, but can get credit if you are**

* Interested applicants should send cover letters, resume, and a copy of their transcript to tgincareers@gmail.com for consideration.

[/vc_column_text][/vc_column][/vc_row]";s:10:"post_title";s:11:"Internships";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:11:"internships";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-09-05 11:24:39";s:17:"post_modified_gmt";s:19:"2018-09-05 11:24:39";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:52:"http://import.thankgoditsnatural.com/?page_id=105074";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}