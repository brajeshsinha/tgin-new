��[<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:105072;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2018-08-29 09:46:33";s:13:"post_date_gmt";s:19:"2018-08-29 09:46:33";s:12:"post_content";s:22997:"[vc_row][vc_column el_class="sec-heading"][vc_column_text]
<h2>Sales/Marketing</h2>
[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]
<h1><strong>Brand Manager – Chicago Only</strong></h1>
We’ve kicked off the search for a Brand Manager who will lead the development of the brand positioning for tgin. You will lead the life cycle brand vision including product research and ideation, go-to-market launch campaigns, continued vertical growth and development, and ongoing consumer research and analysis. You will be responsible for P&amp;L for the vertical and its current and future portfolio of products. Note, this position is both strategic and tactical.

<strong><u>What you’ll do</u></strong>
<ul>
 	<li>Exhibit full ownership of new product development from concept to launch in the hair vertical</li>
 	<li>Lead the vision and drive sustainable market share and profitable growth, based on in-depth competitive landscape and consumer insights</li>
 	<li> Lead the innovation strategy and product development for full size, sample size, set and seasonal products</li>
 	<li>Lead the vision for competitive product positioning to continually reinforce point of difference including assortment architecture, pricing and innovation pipeline</li>
 	<li>Develop category and product playbook including communication dossier, advertising guidelines, features and benefits (functional, technical and emotional), reason to believe, education priorities and competitive positioning</li>
 	<li>Lead the product and ingredient storytelling to amplify brand desirability and differentiation vs mass and prestige competition</li>
 	<li>Manage the current business and own the category vertical P&amp;L, leading action planning and go-to-market strategies for new product launches</li>
 	<li>Develop and ensure execution of brand marketing and 360 degree marketing support calendar based on 12-month launch window</li>
 	<li>Define and develop trade marketing and promotional marketing tools including sampling, sets, POS, displays and other consumer elements</li>
 	<li> Coordinate the department's strategy  with respect to events, sponsorship, video, partnerships, campaigns, print/radio and tv.</li>
 	<li>Work closely with Finance and Strategy to analyze tgin’s existing customer base and target audience. Actively and expertly uses this data to target marketing and improve performance.</li>
 	<li>Prepare and oversees departmental budgets.</li>
</ul>
<strong><u>You’ll love this job if you’re</u></strong>
<ul>
 	<li>A critical thinker. Lead project management with impeccable attention to detail</li>
 	<li>Adaptable and ambitious. You will enthusiastically take on other assignments as needed to support your team!</li>
 	<li>Strong leader. Exhibit initiative and sense of urgency to drive to execution against goals, including on time launches and minimal variance vs. budget</li>
</ul>
<strong><u>What you’ll need</u></strong>
<ul>
 	<li>Bachelor’s degree. MBA is a plus.</li>
 	<li>Minimum 3 years experience in a brand management, product/digital marketing or management consulting in the beauty/cosmetics space</li>
 	<li>Proven track record of building intelligent, data-driven, profit generating strategies</li>
 	<li>Proven track record of configuring, concepting and altering consumer product portfolios</li>
 	<li>Expert knowledge of communicating features, benefits and values to consumers</li>
 	<li>Team player with experience working with engineering, marketing, paid media and creative teams in a fast-paced, agile environment</li>
</ul>
<strong>COMPENSATION: </strong>TBD, but negotiable based on experience.

Please email resume,  cover letter and salary requirements to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>

[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]
<h1><strong>Marketing &amp; Product Development Coordinator - Chicago Only</strong></h1>
<strong><u>Responsibilities</u></strong>
<ul>
 	<li>Identify innovation hunting grounds and new product opportunities</li>
 	<li>Contribute to new product ideation including but not limited to concept, packaging, naming, formula, product launch plans and cost of goods sold</li>
 	<li>Draft new product consumer communications and retail sell-in tools</li>
 	<li>Develop and execute new product positioning statements and write new product concepts.</li>
 	<li>Prepare and circulate New Product Sheets, puts together mock-ups of products, fills out Web Sheets, and ensures all appropriate paper work for tgin products are submitted to the appropriate manager.</li>
 	<li>Assess new product innovation performance to identify key learnings that can be applied to future product launches</li>
</ul>
<strong>COMPENSATION: </strong>TBD, but negotiable based on experience.

Please email resume and cover letter to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>

[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]
<h1 style="font-weight: 400;"><strong>tgin Marketing and Events Coordinator -Chicago Only</strong></h1>
<p style="font-weight: 400;">We are looking for a detail-oriented Marketing and Events coordinator to help support tgin’s event marketing programs. Primary focus for this new role is on tactical planning and execution of marketing events and trade shows. The Marketing and Events  Coordinatorassists in creating brand awareness through advertising, social media, digital and more. This role manages projects, campaigns, and budgets. This individual is also responsible for status reports on marketing efforts, brand analysis, competitive information, and metrics. Must be able to communicate effectively across different levels of the organization, and meet deadlines for multiple projects.<strong> </strong></p>
<p style="font-weight: 400;"><strong><u>Job Requirements</u></strong></p>

<ul>
 	<li style="font-weight: 400;">Assisting team members with day-to-day marketing tasks and coordinating marketing projects and activities as requested</li>
 	<li style="font-weight: 400;">Organize the production of branded items such as stationery, merchandise, giveaways for employee and event use</li>
 	<li style="font-weight: 400;">Support the in-house marketing and design team by coordinating and collating content</li>
 	<li style="font-weight: 400;">Set up tracking systems for marketing campaigns via trade shows and events</li>
 	<li style="font-weight: 400;">Assisting with the production of artwork, sourcing images, print buying and checking copy</li>
 	<li style="font-weight: 400;">Analyze/ benchmark competitor sponsorships and events</li>
 	<li style="font-weight: 400;">Work with Marketing Administrative Assistant to schedule, book and coordinate logistics for events, meetings, trade shows</li>
 	<li style="font-weight: 400;">Support full marketing team with lead generation from events, recording of data, metrics, and staying within budget requirements</li>
 	<li style="font-weight: 400;">This position will entail independent travel, including some weekends, hosting of clients, and management of events – travel requirement 25%.  <strong><em>April – August are our busiest months for travel.</em></strong></li>
</ul>
<p style="font-weight: 400;"><span style="text-decoration: underline;"><strong>Qualifications</strong></span></p>

<ul>
 	<li style="font-weight: 400;">Bachelor's degree</li>
 	<li style="font-weight: 400;"><strong>Ability to travel approximately 25%</strong></li>
 	<li style="font-weight: 400;">2-3 years of marketing, sales or customer service experience</li>
 	<li style="font-weight: 400;">Previous trade show or event planning experience is a bonus, but not required</li>
 	<li style="font-weight: 400;">Demonstrate strong competence in simultaneously managing multiple projects with attention to detail, achieving all deadlines, and performing analysis to determine ROI</li>
 	<li style="font-weight: 400;">Self-motivated and independent</li>
 	<li style="font-weight: 400;">Ability to work in a fast paced environment, with multiple and changing priorities while maintaining strong focus on execution and results.</li>
 	<li style="font-weight: 400;">Excellent written and verbal communication skills, including presentation skills</li>
 	<li style="font-weight: 400;">Strong organizational and planning skills</li>
 	<li style="font-weight: 400;">Detail-oriented with excellent follow-up, budgeting, and time management skills</li>
 	<li style="font-weight: 400;">Exceptional customer service and communication skills</li>
 	<li style="font-weight: 400;">Ability to stand for extended periods of time</li>
 	<li style="font-weight: 400;">Able to problem solve effectively</li>
</ul>
COMPENSATION: TBD, but negotiable based on experience.

Please email resume and cover letter to<a href="mailto:tgincareers@gmail.com"> tgincareers@gmail.com</a>

[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]
<h1><strong>Marketing Coordinator </strong></h1>
Thank God It’s Natural (tgin), a manufacturer of natural hair and body products, is seeking a Marketing Coordinator to become a part of our vibrant Marketing division, working on projects focused in the hair, beauty and fashion industries. We are seeking sharp, enthusiastic candidates with a desire to work on creative projects involving graphic design, motion graphics, video and photography in the beauty industry! This position is ideal for someone who is creative and passionate about marketing, and has the ability to execute and strong attention to detail. A strong preference will be given to candidate with an art background or who have a  degree in Graphic Design, Marketing, Art, Photography, Creative Writing or a related field.  <strong>Candidates must live in Chicago!</strong>

<strong><u>Responsibilities</u></strong>

- Collaborate with creative design teams to drive impressions, word of mouth buzz, social media follows and bottom- line sales
- Design content for multimedia campaigns including collateral materials, video promotions, web promotions, social media marketing and email communications
- Produce creative for event marketing initiatives including (but not limited to) press events, consumer engagement events, product launches, multi-city tours, etc.
- Assist with creative design for new business presentations and campaign reports

<strong><u>Examples of the Projects We’re Currently Working On</u></strong>

- PR/Press Releases
- <a href="http://www.tginatura.com/">Website Sliders</a>
- <a href="https://www.instagram.com/p/BcqhED-h_lb/?taken-by=tginatural"> </a><a href="https://www.instagram.com/p/BcqhED-h_lb/?taken-by=tginatural">Lifestyle Photos</a>
- <a href="https://www.instagram.com/p/Bc0QThhBd1t/?taken-by=tginatural">Commercial Photo Shoots</a>
- <a href="https://www.instagram.com/p/Bb9o9m1BskE/?taken-by=tginatural">Motion Graphics</a>
- Internal Newsletters
- Billboards
- Campaigns (wash n go, fall season, holiday, twitter chats) - See attached photo

<strong><u>knowledge &amp; Abilities/Credentials</u></strong>

- Creative and/or passionate about consumer marketing
-  Working knowledge of PhotoShop, Illustrator, InDesign, PowerPoint-- a must!
- Amazing social media and copy writing skills are a plus!
- Minimum 1-2 years of experience in related fields is ideal!
- Ability to work well under pressure and meet project deadlines
- Strong preference for individuals with (or pursuing) a BS/BA in Graphic Design, Marketing, Art, Photograpy or a related field

<strong><u>Key Personality Traits</u></strong>

- Exceptional communication skills
- Exceptional multitasking
- Passion for customer service and complete satisfaction
- Ability to work both independently and remotely
- Ability to take direction and complete assignments on deadline

* Interested applicants should send cover letter, resumes and requested salary to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a> for consideration.

[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]
<h1><strong>Corporate and Community Relations  – Chicago Only</strong></h1>
We are looking to hire a part-time Corporate Relations to liaise with Chicago corporations and to  facilitate the implementation of projects related to commitment to community relations and being a valued neighbor in those communities that we serve. This position requires close coordination and communication with our Marketing Department.

<strong><u>Responsibilities</u></strong>
<ul>
 	<li>Develop and implement an integrated community relations plan that aligns with the strategic priorities.</li>
 	<li>Work to enhance tgin’s image and reputation internally and externally through partnerships with local schools and cancer related organizations</li>
 	<li>Create and execute plan to develop relationships with key organizations in the communities tgin serves.</li>
 	<li>Manage and select sponsorships and event participation with community partners.</li>
</ul>
<strong><u>Requirements</u></strong>

• Demonstrated leadership, interpersonal, written and oral communication, problem solving, coaching, organizational, and presentation skills.
• Demonstrated evidence of building client relationships and dealing with diverse clients.
• Computer proficiency in MS Office, Outlook, and Internet required.
• Demonstrated evidence of detail-orientation, customer service orientation, ability to work independently, and high levels of responsibility required.
• Ability to travel
• Bachelor’s degree required.
Part-Time

Please email resume, cover letter and hourly pay requirements to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>.

[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]
<h1> <strong>tgin Copywriter - Freelance</strong></h1>
We’re seeking a creative wordsmith with a strong working knowledge of social-media marketing and a passion for the digital space. The Junior Copywriter will write, review and edit review-brand copy for our existing line and new product launches.

<strong>REQUIREMENTS</strong>

<strong><u>Responsibilities</u></strong>
<ul>
 	<li>Work with account teams to develop creative copy for social media, including Facebook, Instagram, and Twitter.</li>
 	<li>Develop creative marketing copy for promotions, digital marketing campaigns, videos, and more</li>
 	<li>Actively seek out and help define new creative possibilities for client engagements and ensure consistency and adherence to corporate brand requirements</li>
 	<li>Excellent verbal and written communication skills</li>
 	<li>Ability to write creatively while managing a consistent workload</li>
 	<li>Must be detail-oriented and know how to solve a problem</li>
 	<li>A desire to work in an exciting and demanding start-up environment</li>
</ul>
<strong>COMPENSATION: </strong>TBD, but negotiable based on experience.

Please email resume, cover letter, 3 writing samples and salary requirements to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>

[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]
<h1><strong>Digital Marketing Manager – Chicago Only</strong></h1>
<strong><u>Responsibilities</u></strong>
<ul>
 	<li>Lead strategy and execution of digital, social, mobile, and PR with an emphasis on inbound marketing strategies to deliver brand engagement.</li>
 	<li>Create sales funnels and track conversions</li>
 	<li>Maintain and monitor project timelines and follow up with crossfunctional team members on an on-going basis</li>
 	<li>Generate customized analysis including online media, search engine campaigns, social metrics, mobile metrics, and web analytics that highlight campaign strengths and weaknesses and provide actionable insights on campaign performance</li>
 	<li>Own certain client accounts and ensure they are running optimally. This includes daily client interaction and service, supervision of a client’s content and advertising, as well as ensuring that work is produced on time and within budget</li>
 	<li>Tracks promotional budget for digitial marketing activity including expenditures versus income.</li>
 	<li>Measure, analyze and report on performance of all digital marketing &amp; PR campaigns deriving actionable insights to improve performance, conversion rates, and loyalty, and increase awareness and trial among core consumer target</li>
 	<li>Analyze market competition regularly including pulling together competitive samples, pricing, and researching selling copy to guide annual operations plans and ongoing business planning.</li>
</ul>
<strong>COMPENSATION: </strong>TBD, but negotiable based on experience.

Please email resume and cover letter to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>

[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]
<h1><strong>tgin Administrative Assistant - Marketing</strong></h1>
<strong><u>Company Overview</u></strong>

Join the creative and energetic <strong>Thank God It’s Natural (tgin)</strong> Marketing Team, and help us further enhance our strong brand. We are a small natural hair business looking to hire a Administrative Assistant to assist our Marketing Team with special projects as we expand our brand across several national retailers. As our Administrative Assistant, you must be meticulous and extremely detailed oriented, comfortable working in a fast pace environment and capable multi-tasking. Experience with Facebook Ads or social media is a major bonus/plus.

<strong><u>Sample Tasks</u></strong>
<ul>
 	<li>Work with graphic designer on updating images</li>
 	<li>Ordering banners from Kinkos</li>
 	<li>Ordering brochures, t-shirts and marketing collateral</li>
 	<li>Planning events according to our event checklist</li>
 	<li>Executing on social media campaigns (e.g. sending prizes to winners)</li>
 	<li>Maintaining a timeline and checklists of ongoing projects</li>
 	<li>Completing event applications</li>
 	<li>Making travel arrangements</li>
 	<li>Following up with team members, vendors, etc. for outstanding items</li>
 	<li>Running Facebook Ads</li>
 	<li>Working with marketing team to develop project plans, schedules, and assign responsibilities</li>
</ul>
<strong><u>Job Responsibilities</u></strong>
<ul>
 	<li>Define and clarify project scope, plan, and schedule.</li>
 	<li>3+ years of experience producing or managing digital and/or experiential projects.</li>
 	<li>Comfort working within fast-paced timelines while managing multiple responsibilities.</li>
 	<li>Strategic thinking skills to identify risks or opportunities during the life of a project.</li>
 	<li>Experience working with clients, vendors and internal teams to ensure the delivery of outstanding projects on time and on budget.</li>
 	<li>Anticipates and overcomes bottlenecks and obstacles to completing projects.</li>
 	<li>Documents project progress and updates senior management regularly and as required by management.</li>
 	<li>Provides periodic reports on project status milestones and adjusts schedules accordingly.</li>
 	<li>Monitors design changes, specifications, and drawing releases and ensures changes are communicated and, where necessary, properly approved.</li>
 	<li>Research vendors and controls expenditures within limitations of project budget.</li>
 	<li>Works diligently to get projects that are off schedule back on schedule to the original due date of the project.  Directly accountable for raising the issue to higher level management that a project is off schedule.</li>
 	<li>Manage Travel Preferences for Air, Hotel, Car, Vacations, Private Charter Flights, &amp; etc.</li>
 	<li>Return Voicemails, Text messages and Emails in addition to Calendar management.</li>
 	<li>Develop policies and procedures to support the achievement of the project objectives.</li>
 	<li>Demonstrate a commitment to tgin core values.</li>
 	<li>Ad hoc responsibilities as needed.</li>
</ul>
<strong><u>Education and Experience</u></strong>
<ul>
 	<li>Strong Written and Oral Communication Skills</li>
 	<li>Extremely Organized</li>
 	<li>Acute multi-tasking skills</li>
 	<li>Flexible and able to Work in a Fast Paced Environment</li>
 	<li>Exceptional Time Management Skills</li>
 	<li>Analytical</li>
</ul>
<strong>COMPENSATION: </strong>TBD, but negotiable based on experience.

Please email resume,  cover letter and salary requirements to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>

[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]
<h1><strong>tgin Brand Ambassadors Wanted for Events + In Store Demos  - Chicago Only</strong></h1>
<div>Our events and promotion team is looking for young, energetic, health conscious women to promote our new tgin Moist Collection for natural hair. A background in retail/sales and a familiarity with natural hair/body products is a plus.</div>
<div>

<strong><u>Job Description</u></strong>

–          Handing Out Samples
–          Selling products and apparel
–          Promoting the Brand at local events (Shecky’s Girls Night Out, African Fest, Taste of Randolph, etc.)
–          Talking about health and the benefits of using natural and organic products
–          Event setup/break down

<strong><u>Ideal Candidate Will Possess</u> </strong>

–          Excellent leadership and communication skills.
–          Strong attention to customer service issues.
–          Prior merchandise planning responsibilities preferred.
–          Detail oriented with the ability to plan, organize and execute corporate goals.
–           Honest
–          Can work in a fast paced environment.
–         Hours: As Needed for Events

Interested applicants should send their resume, cover letter, and three references to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>.  Subject of email should be: TGIN Brand Ambassador.

</div>
[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]
<div class="header">
<div class="logo">
<h1>tgin Hair Stylists to do Platform Work - Chicago Only</h1>
We are looking for experienced natural hair stylists to do platform work demonstrating how our products work at events. Interested applicants should send their<strong> </strong>resume, cover letter, <strong>hourly rate</strong> and sample of their work to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>. Subject of email should be: TGIN Hair Stylist

</div>
</div>
[/vc_column_text][/vc_column][/vc_row]";s:10:"post_title";s:15:"Sales/Marketing";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:15:"sales-marketing";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-09-13 11:03:32";s:17:"post_modified_gmt";s:19:"2018-09-13 11:03:32";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:52:"http://import.thankgoditsnatural.com/?page_id=105072";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}