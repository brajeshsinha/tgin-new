��[<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:376;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2016-04-12 14:34:51";s:13:"post_date_gmt";s:19:"2016-04-12 14:34:51";s:12:"post_content";s:11652:"[vc_row type="in_container" full_screen_row_position="middle" scene_position="center" text_color="dark" text_align="left" overlay_strength="0.3"][vc_column column_padding="no-extra-padding" column_padding_position="all" background_color_opacity="1" background_hover_color_opacity="1" el_class="sec-heading" tablet_text_alignment="default" phone_text_alignment="default"][vc_column_text]
<h2>our founder</h2>
[/vc_column_text][/vc_column][/vc_row][vc_row type="in_container" full_screen_row_position="middle" scene_position="center" text_color="dark" text_align="left" overlay_strength="0.3"][vc_column width="1/2" column_padding="no-extra-padding" column_padding_position="all" background_color_opacity="1" background_hover_color_opacity="1" tablet_text_alignment="default" phone_text_alignment="default"][vc_single_image image="105113" img_size="full"][/vc_column][vc_column width="1/2" column_padding="no-extra-padding" column_padding_position="all" background_color_opacity="1" background_hover_color_opacity="1" tablet_text_alignment="default" phone_text_alignment="default"][vc_column_text]
<p style="text-align: justify;"><strong>Chris-Tia Donaldson</strong> is the Founder and Chief Executive Officer of Thank God It’s Natural (tgin), a manufacturer of natural hair and skin care products, currently sold in Walmart, Target, Whole Foods, Sally’s Beauty, and Walgreen's.  In her role, she oversees all aspects of day-to-day operations, sales, and strategic partnerships.</p>
<p style="text-align: justify;">Chris-Tia has been featured in major media publications such as USA Today, Marie Claire, Essence, Black Enterprise, Ebony, Heart &amp; Soul, and the Chicago Tribune. Her book Thank God I’m Natural: The Ultimate Guide to Caring for Natural Hair is a #1 Amazon bestseller, and was hailed the “Natural Hair Bible” by Essence Magazine.</p>
<p style="text-align: justify;">Prior to starting her own company, Chris-Tia represented Fortune 500 companies in complex business transactions involving technology and open source code.  Chris-Tia earned her A.B. in Economics from Harvard University with high honors, and is a graduate of Harvard Law School.</p>
<p style="text-align: justify;">In 2015, Chris-Tia was diagnosed with breast cancer.  During her treatment, she learned that <strong>having money could make the difference between living and dying</strong> when it came to treating this conditioner. In her observation, few organizations existed that provided support and social services to help women with transportation, child care, parking, or seeking disability leave from their place of employment.</p>
<p style="text-align: justify;">Today, she uses her success in the beauty space to advocate for women experiencing financial difficulties, who are undergoing treatment, to highlight health disparities due to race and socio-economic factors, and to empower women to listen to their bodies through the <a href="https://thankgodimnatural.com/tgin-foundation/">tgin Foundation</a>.</p>
Chris-Tia is currently penning her next book, which she plans to release in the summer of 2018, called <em>This is Only a Test: What Breast Cancer Taught me about Faith, Love, Hair and Business.</em>
<p class="p4"><span class="s1">For updates about Chris-Tia, follow her on:</span></p>
<p class="p5"><span class="s2">Facebook (<a href="http://www.facebook.com/thankgodimnatural"><span class="s3">www.facebook.com/thankgodimnatural</span></a>)
Twitter (<a href="https://twitter.com/tginceo?lang=en"><span class="s3">@tginceo</span></a>)
Instagram (<a href="http://instagram.com/tginceo"><span class="s3">@tginceo</span></a>)</span></p>
[/vc_column_text][/vc_column][/vc_row][vc_row type="in_container" full_screen_row_position="middle" scene_position="center" text_color="dark" text_align="left" overlay_strength="0.3" css=".vc_custom_1461144216469{margin-top: 50px !important;}"][vc_column column_padding="no-extra-padding" column_padding_position="all" background_color_opacity="1" background_hover_color_opacity="1" el_class="sec-heading" tablet_text_alignment="default" phone_text_alignment="default"][vc_column_text][divider line_type="Full Width Line" custom_height="20"]

&nbsp;

https://youtu.be/BnOF7g-ZZts

&nbsp;

[divider line_type="Full Width Line" custom_height="20"]
<h2>Interview with Chris-Tia</h2>
[/vc_column_text][/vc_column][/vc_row][vc_row type="in_container" full_screen_row_position="middle" scene_position="center" text_color="dark" text_align="left" overlay_strength="0.3" css=".vc_custom_1461144954302{margin-top: 10px !important;}"][vc_column column_padding="no-extra-padding" column_padding_position="all" background_color_opacity="1" background_hover_color_opacity="1" tablet_text_alignment="default" phone_text_alignment="default"][vc_column_text]
<p style="text-align: justify;"><em>by: Jamie Fleming-Dixon of Fly Female and Fabulous</em></p>
<strong>How did you become interested in natural hair/natural hair care?</strong>
<p style="text-align: justify;">I became interested in natural hair care back in 2002 when there were very few products on the market designed for kinky, curly and wavy textures. This was before YouTube, Facebook and Instagram, so there wasn’t really a lot of information on the topic. Moreover, there was a pervasive belief that long straight hair was something that women should strive to attain, and that kinky, curly or wavy hair was a curse of sorts or something to be ashamed of.  As a result, I decided to write a book to help dispel some of the most common myths and misconceptions  associated with this hair texture.</p>
<p style="text-align: justify;">At the same time, I always had it in the back of my mind that I wanted to launch my own product line, because very few products on the market actually catered to dry natural hair, and some of the prices were just outrageous. So I wanted to come out with a line of products that were not only reasonably priced, but delivered on performance and left the wearer with softer, moisturized and more manageable curls. I’m glad to say that after years of research, we’ve been pretty successful in achieving this goal and now our products can actually be found in Walgreen’s or online at <a href="http://www.tginstore.com/">www.tginstore.com</a>.</p>
<p style="text-align: justify;"><strong>What is your role as the CEO of Thank God I’m Natural?</strong>
I’m over everything related to daily operations. That means I oversee manufacturing/production, research and development, sales, marketing, social media, legal, etc. We have a great team of people who work for us who actually execute on these areas, but my job is to come up with ideas for how we can make the business run more efficiently.</p>
<p style="text-align: justify;"><strong>Why did you choose entrepreneurship?</strong><strong>
</strong>I like entrepreneurship, because it’s a constant challenge. Everyday you wake up ready to tackle a new and exciting problem, whether it’s should we offer sample sizes, or upgrade our formulas to include argan oil.  Every day is exciting. It also allows me to create and leave my family with a legacy, while also giving back to my community and helping to create jobs.</p>
<p style="text-align: justify;"><strong>What do you love most about owning your own business?</strong><strong>
</strong>The challenge and constant problem solving is awesome. I also love getting to meet and work closely with people who I would otherwise have no contact with.</p>
<p style="text-align: justify;"><strong>What do you find to be most stressful?</strong><strong>
</strong>Production. Right now 3 of our hair skus are made in the lab. 5 of the skus are made by us in house, which means we have to constantly track inventory and make sure we have enough of everything, which can be pretty taxing because we’re always running out of something due to the increased demand for our products. Financing and cash flow can also be stressful. There are days where we can spend $2,000 just on product labels, and you think to yourself like wow that’s a mortgage or someone’s salary for the month. So although we’re making money, you can’t wait for the moment when you can really get your head above water.</p>
<p style="text-align: justify;"><strong>What would you say is the bigggest lesson you’ve learned so far as an</strong><strong> entrepreneur?
</strong>Use other people’s money. Chase, AMEX, etc. Meaning, I wish I would have relied on more third party sources for financing in the beginning. Also, hiring a book keeper has been one of the best decisions I’ve ever made. Whenever an opportunity arises, where an investor or a bank or a third party, needs to review our financial statements, they are all ready to go. Also, having the numbers in front of me every month allows me to review different line items to figure out how we can increase our profitability.</p>
<p style="text-align: justify;"><strong>What are the most important skills and characteristics a person needs to do what you do?
</strong>You have to be persistent and have a really good work ethic.  You must also know how to take things one day at a time.</p>
<strong>
What are three things that have contributed to your success as an entrepreneur?</strong>
<ol style="text-align: justify;">
 	<li>Having a law degree has given me the ability to be extremely detailed oriented. When you’re manufacturing products, there are a ton of people involved in every stage, and you need to be able to keep track of all of the little details (e.g. bottle size, label copy, INCI statements, bar codes, etc.). One small mistake can cost you thousands of dollars, so I’ve been pretty lucky up until this point.</li>
 	<li>Persistence – many people are amazed at all that I’ve accomplished in such a short period of time, but I’ve been working this hard since high school so its nothing new to me. I don’t know life any other way.</li>
 	<li>My faith in God – I know what I’m trying to do with this natural hair care business is a major undertaking, and I feel like my faith and God has gotten me to this point and will continue to help us chart our course in this space over the next few years.</li>
</ol>
<p style="text-align: justify;"><strong>What is your favorite inspirational quote or saying?</strong></p>
Biblical –

<em>“Trust in the lord with all of your heart; and lean not to your own understanding (Proverbs 3:5)”</em>

Business –

<em>“Perfect is the enemy of good enough.”</em>
<p style="text-align: justify;"><strong>How do you stay encouraged?</strong><strong>
</strong>I just take things one day at a time, and stay prayerful. I know God would not give me this assignment if he didn’t think I could pull it off.</p>
<strong>Do you have any advice for women looking to start or grow their businesses?</strong><strong>
</strong>Trust your instincts.
Work Hard.
And Just Start.
<p style="text-align: justify;"><strong>What tips do you have for women on their journeys to success?
</strong>I think a lot of people have a passion, but don’t know how to turn their passion into a profitable enterprise. And my biggest advice would be to just start. You’ll make mistakes, but over time things get easier and clearer. One of the hardest parts of starting a business is just starting it. Many people have a project that has been burning in their heart for years, but if they would just start researching and doing, they would find that God has equipped them with everything they need to be successful.</p>
[/vc_column_text][/vc_column][/vc_row]";s:10:"post_title";s:11:"Our Founder";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:9:"our-story";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-11-19 13:39:28";s:17:"post_modified_gmt";s:19:"2018-11-19 13:39:28";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:46:"http://thankgodimnatural.com/tgin/?page_id=373";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}