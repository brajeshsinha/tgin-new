��[<?php exit; ?>a:1:{s:7:"content";s:36818:"a:2:{s:6:"Latest";s:5:"2.0.5";s:9:"changelog";s:36760:"

<h4>Version 2.0.5 - Released: Oct 23, 2018</h4>
<ul>
    <li>Update: plugin framework</li>
    <li>Update: plugin description</li>
    <li>Update: plugin links</li>
    <li>Update: updating language files</li>
    <li>Update: Updating plugin description</li>
    <li>Dev: added new filter yith_ywgc_pdf_new_file_path</li>
</ul>
<h4>Version 2.0.4 - Released: Oct 17, 2018</h4>
<ul>
    <li>New: Support to WooCommerce 3.5.0</li>
    <li>New:  Basic integration with WooCommerce Smart Coupons</li>
    <li>Tweak: Allow to use the coupons form with a gift card code activating a filter</li>
    <li>Tweak: Aelia compatibility get the order total in actual currency</li>
    <li>Tweak: new action links and plugin row meta in admin manage plugins page</li>
    <li>Tweak: customize the button label for the gift card email and show it always even without applying disscount</li>
    <li>Update: Updated the language files</li>
    <li>Update: updated the Norwegian language, thanks to Jørgen Eggli.</li>
    <li>Update: Updated the plugin-FW</li>
    <li>Fix: email button add to cart automatically for non gift this product</li>
    <li>Fix: not show "x" image when logo is not selected</li>
    <li>Fix: fixing a string error</li>
    <li>Dev: Added new filter yith_ywgc_email_automatic_discount_text</li>
    <li>Dev: added a new action yith_ywgc_before_disallow_gift_cards_with_same_title_query</li>
    <li>Dev: added filter yith_ywgc_gift_card_orders_total</li>
    <li>Dev: added new arg to the yith_wcgc_template_after_message filter</li>
    <li>Dev: check if is_array, prevent errors with php "Count" function</li>
    <li>Dev: added a filter 'ywgc_empty_recipient_note'</li>
    <li>Dev: filters for images on the pdf</li>
    <li>Dev: new filter yith_wcgc_gift_card_details_mandatory_recipient</li>
    <li>Dev: javascript minify of frontend</li>
    <li>Dev: filter to display gift_amounts as a select2 of jquery</li>
</ul>
<h4>Version 2.0.3 - Released: Sep 19, 2018</h4>
<ul>
    <li>Tweak: Compatibility with new Aelia version (4.6.5.180828)</li>
    <li>Tweak: Set gift card custom post type as exportable</li>
    <li>Tweak: filter to customize the "add to card" of the gift card</li>
    <li>Tweak: text changed for better understanding</li>
    <li>Tweak: Manual amount option displayed even with only one gift card price</li>
    <li>Update: .pot main language file</li>
    <li>Update: Spanish translation</li>
    <li>Update: Dutch language>/li>
    <li>Update: Italian language</li>
    <li>Fix: Add price with WooCommerce configuration taxes to the cart</li>
    <li>Dev: remove pdfs after sending</li>
    <li>Dev: Race conditions - gift card duplicated</li>
    <li>Dev: adding admin panel javascript minimized</li>
</ul>
<h4>Version 2.0.2 - Released: Sep 04, 2018</h4>
<ul>
    <li>New: New options for the form to apply the gift card code on the cart and checkout page</li>
    <li>Dev: gift this product on PHP Unit</li>
</ul>
<h4>Version 2.0.1 - Released: Sep 03, 2018</h4>
<ul>
    <li>New: pdf attached to the gift card code email</li>
    <li>New: Allow buyers to receive a BCC email with the gift card code</li>
    <li>Tweak: Hide default gift card product on the admin products</li>
    <li>Tweak: Check if $product exist for prevent fatal error</li>
    <li>Tweak: Display parent image if variation product does not have image</li>
    <li>Tweak: Adding the gif loader when choosing image to gift</li>
    <li>Update: Dutch translation</li>
    <li>Fix: Displaying product image for variable products</li>
    <li>Fix: add the customize label for "Gift this product" for variable products</li>
    <li>Fix: fixing wrong string</li>
    <li>Dev: comment of how to activate the internal note in the list of gift cards</li>
    <li>Dev: cleaning error_log code</li>
    <li>Dev: added a new filter in the check gift card return</li>
    <li>Dev: displaying notes column on the list of Gift Cards</li>
</ul>
<h4>Version 2.0.0 - Released: Aug 09, 2018</h4>
<ul>
    <li>New: Settings Admin panel</li>
    <li>Tweak: plugin action links and row metas</li>
    <li>Tweak: prevent error with gift this product button on shop loop</li>
    <li>Tweak: added new status to the gift card table</li>
    <li>Tweak: Check the recipient names as array</li>
    <li>Update: Spanish translation</li>
    <li>Update: Dutch translation</li>
    <li>Updated: official documentation url of the plugin</li>
    <li>Fix: Fixed a issue with the AJAX Call in Frontend</li>
    <li>Fix: string gft to gift on privacy policy content</li>
    <li>Fix: Change the hook of the gift this product button in variable products</li>
    <li>Dev: adding the product to the gift this product template</li>
    <li>Dev: fixed the filter to the variation products with gift this product button</li>
    <li>Dev: checking YITH_Privacy_Plugin_Abstract for old plugin-fw versions</li>
    <li>Dev: adding new filters in the checkout order tables</li>
    <li>Dev: PHPUnit</li>
</ul>
<h4>Version 1.8.5 - Released: May 28, 2018</h4>
<ul>
    <li>New: Support to GDPR compliance</li>
    <li>New: Support to WooCommerce 3.4.0</li>
</ul>
<h4>Version 1.8.4 - Released: Feb 12, 2018</h4>
<ul>
	<li>New: added a filter to register the script URL</li>
	<li>Fix: hide select in case of single amount in product page</li>
	<li>Fix: error when try to upload an image and the limit is 0</li>
	<li>Dev: added a new filter in to the shop url in email button</li>
</ul>
<h4>Version 1.8.3 - Released: Feb 07, 2018</h4>
<ul>
	<li>Tweak: Checking if the gift card is object</li>
	<li>Fix: Aelia compatibility when adding to the cart selecting amount of a gift card and gift this product</li>
	<li>Fix: Wrong calculation on shipping total row</li>
</ul>
<h4>Version 1.8.2 - Released: Jan 29, 2018</h4>
<ul>
	<li>New: support to WooCommerce 3.3-RC2</li>
	<li>New: show gift card code in order detail page inside my account section</li>
	<li>New: plugin fw version 3.0.10</li>
	<li>Update: french language</li>
	<li>Tweak: Adding 'use product image' for virtual gift cards</li>
	<li>Dev: new filter 'ywgc_remove_gift_card_text'</li>
	<li>Dev: new filter 'ywgc_checkout_enter_code_text'</li>
	<li>Fix: fatal error showing quantity table with Dynamic Pricing and Discounts plugin</li>
	<li>Fix: fatal error get_default_subject (for WooCommerce version minor of 3.1)</li>
</ul>
<h4>Version 1.8.1 - Released: Dec 21, 2017</h4>
<ul>
	<li>New: Danish language</li>
	<li>New: Apply filters yith_ywgc_give_product_as_present</li>
	<li>Update: Plugin core framework version 3</li>
	<li>Update: French language files</li>
	<li>Dev: filter yith_ywgc_preset_image_size</li>
	<li>Dev: added second argument $args on filter yith_ywgc_email_automatic_cart_discount_url</li>
	<li>Dev: Create YWGC_META_GIFT_CARD_CODE for a order created</li>
	<li>Dev: new filter "yith_ywgc_update_totals_calculate_taxes"</li>
	<li>Dev: override method get_subject() inside class YITH_YWGC_Email_Send_Gift_Card</li>
	<li>Fix: PayPal error when gift discount can’t pay total shipping costs</li>
	<li>Fix: use the featured image when no image selected</li>
	<li>Fix: usage of nl2br() function to show the gift card message</li>
	<li>Fix: subscription totals</li>
	<li>Fix: Compatibility with YITH WooCommerce Subscription Premium</li>
	<li>Fix: error_log in code</li>
	<li>Fix: Compatibility Aelia: Converting the price in the gift cards of my account</li>
	<li>Fix: Remove “selected more than one recipient” for Safari</li>
</ul>
<h4>Version 1.8.0 - Released: Nov 23, 2017</h4>
<ul>
	<li>New: Adding recipient details to physical gift cards</li>
	<li>Fix: Avoid new gift cards with the same name</li>
	<li>Update: Dutch translations</li>
	<li>Fix: Gift card default hidden product is indexed by Google</li>
</ul>
<h4>Version 1.7.13 - Released: Nov 20, 2017</h4>
<ul>
	<li>New: Norwegian translations thanks to Rune Kristoffersen</li>
	<li>Fix: Coupons behaviour</li>
</ul>
<h4>Version 1.7.12 - Released: Nov 16, 2017</h4>
<ul>
	<li>Fix: Coupons not correctly applied with gift cards in cart and checkout</li>
	<li>New: Partial French translations thanks to Yaël Fazy</li>
	<li>Fix: Warning on class-yith-ywgc-backend-premium.php with some php configuration</li>
</ul>
<h4>Version 1.7.11 - Released: Nov 15, 2017</h4>
<ul>
	<li>Fix: PHP Fatal error "Call to undefined method WC_Cart::get_shipping_tax()"</li>
</ul>

<h4>Version 1.7.10 - Released: Nov 15, 2017</h4>
<ul>
	<li>New: German translation (thanks to Wolfgang Männel)</li>
	<li>New: Dutch translation</li>
	<li>Dev: new filter 'ywgc_email_image_size'</li>
	<li>Fix: gift card discount is not calculate correctly</li>
	<li>Fix: scroll on choose design modal window</li>
</ul>
<h4>Version 1.7.9 - Released: Nov 10, 2017</h4>
<ul>
	<li>Fix: shipping totals not included in gift card discount</li>
</ul>
<h4>Version 1.7.8 - Released: Nov 09, 2017</h4>
<ul>
	<li>Tweak: My Account message when no gift cards found</li>
	<li>Fix: Zero amount display for zero balance gift cards</li>
	<li>Fix: Empty 'used' tab in Gift Cards backend panel</li>
	<li>Fix: Gift cards not applied to shipping taxes</li>
	<li>Fix: Error message when try to use a zero balance gift card</li>
</ul>
<h4>Version 1.7.7 - Released: Nov 08, 2017</h4>
<ul>
	<li>New: gift-card endpoint to my-account area</li>
	<li>Update: Moved Gift Cards from my-account dashboard to the new gift-card area</li>
	<li>Fix: Pretty Photo modal position on Gift Card product page</li>
	<li>Fix: Allowing the administrator to use the product image when gifting a product</li>
	<li>Update: Spanish language files</li>
</ul>
<h4>Version 1.7.6 - Released: Nov 08, 2017</h4>
<ul>
	<li>Add: filter ywgc_preview_code_title</li>
	<li>Add: filter ywgc_design_section_title</li>
	<li>Add: filter ywgc_checkout_box_title</li>
	<li>Add: filter ywgc_checkout_box_placeholder</li>
	<li>Add: filter ywgc_checkout_apply_code</li>
	<li>Fix: Zero amount warning for current balance on PHP 7.1.X</li>
</ul>
<h4>Version 1.7.5 - Released: Nov 07, 2017</h4>
<ul>
	<li>Fix: field "name" not showed creating manually a new gift card</li>
	<li>Fix: missed string in yith-woocommerce-gift-cards.pot file</li>
	<li>Update: italian language file</li>
	<li>Update: spanish language file</li>
</ul>
<h4>Version 1.7.4 - Released: Oct 27, 2017</h4>
<ul>
	<li>Fix: Zero amount displayed in gift cards with multiple amounts</li>
</ul>
<h4>Version 1.7.3 - Released: Oct 26, 2017</h4>
<ul>
	<li>Fix wrong amount added to cart in case of thousands</li>
</ul>
<h4>Version 1.7.2 - Released: Oct 25, 2017</h4>
<ul>
	<li>Fix: price format gift card</li>
	<li>Fix: unminified js file was not updated</li>
</ul>

<h4>Version 1.7.1 - Released: Oct 24, 2017</h4>
<ul>
	<li>New: use product image as gift card image</li>
	<li>Dev: new filter 'yith_ywcgc_attachment_image_url'</li>
	<li>Fix: custom image is not showed on gift card</li>
</ul>


<h4>Version 1.7.0 - Released: Oct 17, 2017</h4>
<ul>
	<li>New: support to WooCommerce 3.2.1</li>
	<li>Updated: language files</li>
	<li>Fix: shop page URL link to get auto discount button</li>
	<li>Fix: gift card code pattern is not set by default</li>
	<li>Fix: Php warning and notices on canceled order without gift cards applied if paying with 2checkout</li>
	<li>Fix: default gift card image is not recovered correctly</li>
	<li>Fix: PayPal error if the shipping total is higher than cart subtotal</li>
	<li>Fix: issue layout when removing a coupon</li>
	<li>Fix: gift card expiration is not set for gift card created manually</li>
	<li>Fix: same name for multiple recipient emails</li>
	<li>Dev: new hook 'yith_wcgc_template_after_logo'</li>
	<li>Dev: new hook 'yith_wcgc_template_after_main_image'</li>
	<li>Dev: new hook 'yith_wcgc_template_after_amount'</li>
	<li>Dev: new hook 'yith_wcgc_template_after_code'</li>
	<li>Dev: new hook 'yith_wcgc_template_after_message'</li>
	<li>Tweak: scheduling cron to check scheduled gift card</li>
</ul>	
<h4>Version 1.6.16 - Released: Sep 19, 2017</h4>
<ul>
    <li>New: "expiration date" column in gift card table</li>
    <li>Tweak: button position inside product detail page</li>
    <li>Fix: incorrect gift card amount if all variations have same price and no variation is set as default</li>
    <li>Dev: add $gift_card as parameter for filter 'yith_ywgc_gift_card_email_expiration_message'</li>
    <li>Dev: new filter 'yith_wcgc_date_format'</li>
</ul>   

<h4>Version 1.6.15 - Released: Sep 12, 2017</h4>
<ul>
	<li>Fix: wrong order total when the order is saved</li>
	<li>Fix: incorrect gift card amount if all variations have same price</li>
	<li>Dev: new filter ywgc_amount_order_total_item</li>
	<li>Dev: new filter 'ywgc_sender_name_label'</li>
	<li>Dev: new filter 'ywgc_sender_name_value'</li>
	<li>Dev: new filter 'ywgc_edit_message_label'</li>
	<li>Dev: new filter 'ywgc_edit_message_placeholder'</li>
	<li>Dev: new filter 'ywgc_postdated_field_label'</li>
	<li>Dev: new filter 'ywgc_choose_delivery_date_placeholder'</li>
	<li>Dev: new filter 'ywgc_cancel_gift_card_button_text'</li>
</ul>	
<h4>Version 1.6.14 - Released: Sep 07, 2017</h4>
<ul>
	<li>Fix: update gift card balance when order status is cancelled or refunded</li>
</ul>	
<h4>Version 1.6.13 - Released: Aug 31, 2017</h4>
<ul>
    <li>Fix: order item meta warning</li>
	<li>New: filter 'yith_wcgc_gift_this_product_button_label'</li>
	<li>New: filter 'yith_wcgc_manual_amount_input_placeholder'</li>
	<li>New: filter 'yith_wcgc_manual_amount_option_text'</li>
</ul>
<h4>Version 1.6.12 - Released: Aug 24, 2017</h4>
<ul>
    <li>Tweak: order item meta is shown as string</li>
    <li>Update: language files</li>
</ul>
<h4>Version 1.6.11 - Released: Aug 17, 2017</h4>
<ul>
    <li>Add: Filters on Gift Card Post Type admin page</li>
</ul>
<h4>Version 1.6.10 - Released: Aug 09, 2017</h4>
<ul>
    <li>Fix: Wrong amount shown in cart page when using Aelia Currency Switcher plugin</li>
    <li>Fix: 'Add another recipient' string not shown in gift card page due a typo in gift-card-details.php</li>
</ul>
<h4>Version 1.6.9 - Released: Jul 03, 2017</h4>
<ul>        
    <li>New: Support to WooCommerce 3.1</li>
    <li>Fix: disable 'gift this product' button when a variation is not selected</li>
</ul>
<h4>Version 1.6.8 - Released: Jun 19, 2017</h4>
<ul>    
    <li>Update: improved the layout of gift this product section for variable products</li>
    <li>Fix: wrong CSS prop set in ywgc-frontend.js</li>
    <li>Fix: gift card image is shown and then hide on product's page when 'Gift this product' option is enabled</li>
</ul>
<h4>Version 1.6.7 - Released: May 19, 2017</h4>
<ul>    
    <li>New: added 'D' placeholder in gift card code pattern, it will replaced by a digit in place of a random letter</li>
    <li>Fix: missing amount conversion with Aelia Currency Switcher and WooCommerce 3</li>
    <li>Fix: prevent a Javascript error when WooCommerce trigger 'found_variation' and the variation object is not set</li>
    <li>Fix: avoid a fatal error when activating the plugin on website with PHP prior than 5.4</li>
</ul>
<h4>Version 1.6.6 - Released: May 08, 2017</h4>
<ul>    
    <li>Fix: 'choose design' button does nothing when clicked, showing the modal window when clicked again</li>
    <li>Fix: 'choose design' not visible when using the 'gift this product' feature</li>
    <li>Tweak: improved layout for preset design modal window</li>
</ul>
<h4>Version 1.6.5 - Released: Apr 27, 2017</h4>
<ul>    
    <li>Update: plugin language file.</li>
    <li>Tweak: improved the gift card layout.</li>
    <li>Fix: 'Add gift' text not localizable.</li>
    <li>Fix: billing_first_name property called directly in WooCommerce 3.</li>
</ul>
<h4>Version 1.6.4 - Released: Apr 21, 2017</h4>
<ul>    
	<li>Update: plugin-fw</li>
	<li>Fix: using 'Gift this product' feature do not add the product to the cart due to product not purchasable</li>
	<li>Fix: fatal error due to huge amount of post meta</li>
</ul>
<h4>Version 1.6.3 - Released: Apr 14, 2017</h4>
<ul>    
	<li>Fix: 'missing recipient' notice shown when purchasing a physical gift card</li>
	<li>Fix: broken compatibility with Aelia Currency Switcher</li>
	<li>Fix: usage notification not sent to the buyer in case of physical gift card</li>
	<li>Fix: prevent email sending duplicates</li>
</ul>
<h4>Version 1.6.2 - Released: Apr 11, 2017</h4>
<ul>    
	<li>New: compatible with both WPML "Product Translation Interface" mode</li>
	<li>New: template /single-product/add-to-cart/gift-card-add-to-cart.php</li>
	<li>Update: language files</li>
	<li>Tweak: purchasable status depends on gift card amounts and to the manual amount status</li>
	<li>Tweak: gift card template not shown on gift card product page if the product is not purchasable</li>
	<li>Fix: digital gift card shown as physical product on back end when used with WooCommerce 3.0 or newer</li>
</ul>
<h4>Version 1.6.1 - Released: Mar 28, 2017</h4>
<ul>
    <li>Fix: removed 'debugger' on front end script</li>
	<li>Fix: updated minified front end script.</li>
</ul>
<h4>Version 1.6.0 - Released: Mar 27, 2017</h4>
<ul>
    <li>New: Support WooCommerce 3.0
    <li>New: apply gift card to the cart totals after applying coupon and calculating taxes</li>
    <li>New: register gift cards usage as order notes</li>
    <li>New: set your own pattern for the gift card code to generate</li>
    <li>New: if the gift card code pattern is not so complex to allow unique code, the gift card code will be created but will be not valid until a manual code is entered</li>
    <li>New: avoid unique code conflicts for new or edited gift cards</li>
    <li>Fix: YITH Plugin Framework initialization</li>
    <li>Fix: gift card usage notification not sent to the buyer</li>
    <li>Remove: option to choose if gift card discount should applies to shipping fee as it is the default behavior</li>
    <li>Remove: gift card code can no longer be entered in the coupon field</li>
    <li>Remove: gift cards are no more linked to WooCommerce coupon system</li>
</ul>
<h4>Version 1.5.16 - Released: Feb 17, 2017</h4>
<ul>
	<li>New: add internal notes to gift cards and show them on gift cards table.</li>
	<li>Fix: notice shown when 'free shipping' is the selected shipping method.</li>
	<li>Fix: custom image set from edit product page not saved correctly.</li>
	<li>Fix: the order action for sending gift cards not triggered.</li>
	<li>Fix: wrong amount shown on gift card product page when using third party plugin for currency switching.</li>
	<li>Fix: amount not shown on admin product page when added to the gift card product, needing a page refresh.</li>
	<li>Dev: customize the gift card's parameters with the filter 'yith_ywgc_gift_card_coupon_data' when the gift card code is applied to the cart.</li>
</ul>
<h4>Version 1.5.15 - Released: Feb 09, 2017</h4>
<ul>
    <li>Fix: scheduled gift card email sent to the recipient even if already delivered</li>
</ul>
<h4>Version 1.5.14 - Released: Feb 07, 2017</h4>
<ul>
    <li>Update: gift-card-details.php template</li>
	<li>Update: language files</li>
	<li>Tweak: in gift card product page, the 'quantity' field and 'add to cart' button are moved under the gift card area</li>
	<li>Fix: scheduled date for gift cards was not set correctly during the purchase</li>
	<li>Fix: gift card design not enabled for products translated with WPML</li>
	<li>Fix: gift card manual amount not enabled for products translated with WPML</li>
</ul>
<h4>Version 1.5.13 - Released: Feb 02, 2017</h4>
<ul>
    <li>New: show message about the expiration date in gift card email</li>
    <li>Fix: gift card amounts shown on products translated with WPML</li>
</ul>
<h4>Version 1.5.12 - Released: Feb 01, 2017</h4>
<ul>
    <li>New: show gift card expiration date in customer gift card table</li>
    <li>Fix: issues with gift card expiration date</li>
    <li>Fix: issues with shipping of scheduled gift cards</li>
    <li>Dev: yith_ywgc_my_gift_cards_columns filter lets third-party plugins customize columns shown on customer gift card table</li>
</ul>
<h4>Version 1.5.11 - Released: Jan 27, 2017</h4>
<ul>
    <li>Fix: gift card amount ordering was not sorted correctly</li>
    <li>Fix: wrong image on gift cart product pages used when the user clicks on 'default image' button</li>
</ul>
<h4>Version 1.5.10 - Released: Jan 18, 2017</h4>
<ul>
	<li>New: choose if the image title should be shown in design list pop-up</li>
	<li>Dev: the filter 'yith_ywgc_gift_cards_amounts' lets third party plugin to change the content of the amounts dropdown</li>
</ul>
<h4>Version 1.5.9 - Released: Jan 13, 2017</h4>
<ul>
	<li>New: template automatic-discount.php in /templates/emails</li>
	<li>Tweak: footer content in emails can be customized</li>
</ul>
<h4>Version 1.5.8 - Released: Jan 03, 2017</h4>
<ul>
	<li>Fixed: gift card with empty recipient email not added to the cart</li>
</ul>
<h4>Version 1.5.7 - Released: Jan 02, 2017</h4>
<ul>
	<li>Added: gift cards with multi-recipient will be added to the cart one per row</li>
	<li>Added: choose if the recipient email should be shown in cart details</li>
</ul>
<h4>Version 1.5.6 - Released: Dec 22, 2016</h4>
<ul>
	<li>Added: email format validation before adding the gift card to the cart</li>
	<li>Added: set gift card expiration</li>
	<li>Tweaked: improved cart messages when a gift card code is used</li>
	<li>Fixed: gift card email not send to the admin email when BCC option is set</li>
</ul>    
<h4>Version 1.5.5 - Released: Dec 21, 2016</h4>
<ul>
	<li>Fixed: wrong amount shown in variable products with 'Gift this product' option</li>
	<li>Fixed: missing currency conversion with Aelia Currency Switcher</li>
	<li>Fixed: sender name not shown in product suggestion template</li>
	<li>Fixed: amount not shown in user currency in email</li>
	<li>Fixed: WPML currency for gift cards</li>
	<li>Fixed: multiple gift cards not added to the cart correctly</li>
</ul>
<h4>Version 1.5.4 - Released: Dec 15, 2016</h4>
<ul>
    <li>Fixed: gift card first usage email should be sent to the customer instead of the gift card recipient</li>
    <li>Fixed: the real gift card image was not emailed to the recipient</li>
</ul>
<h4>Version 1.5.3 - Released: Dec 14, 2016</h4>
<ul>
    <li>Fixed: recipient email validation on gift card page</li>
</ul>
<h4>Version 1.5.2 - Released: Dec 13, 2016</h4>
<ul>
    <li>Fixed: 'Send now' not working on manual created digital gift cards</li>
</ul>
<h4>Version 1.5.1 - Released: Dec 07, 2016</h4>
<ul>
    <li>Added: ready for WordPress 4.7</li>
</ul>
<h4>Version 1.5.0 - Released: Dec 06, 2016</h4>
<ul>    
	<li>Added: gift cards can be create manually from back end</li>
	<li>Added: gift cards balance can be managed from back end</li>
	<li>Added: gift cards details can be edited from back end</li>
	<li>Added: create your own gift card template overwriting the plugin templates</li>
	<li>Added: allow inventory for gift card products</li>
	<li>Added: allow 'sold individually' for gift cards</li>
	<li>Added: choose if in digital gift cards, the recipient email is mandatory</li>
	<li>Added: recipient name in gift card product template</li>
	<li>Updated: gift card code are generated as soon as possible after the payment of the order(in 'processing' or 'completed' order status)</li>
	<li>Updated: the sender name is no more mandatory when a digital gift cards is purchased</li>
	<li>Fixed: various issues with Aelia Currency Switcher</li>
</ul>
<h4>Version 1.4.13 - Released: Nov 18, 2016</h4>
<ul>
    <li>Fixed: a console log shown when manual amount option is not set</li>
    <li>Updated: plugin language files</li>
</ul>
<h4>Version 1.4.12 - Released: Nov 16, 2016 </h4>
<ul>
    <li>Added: gift cards can be used for paying shipping cost</li>
    <li>Updated: improved checks on the amount entered by the customer in manual amount mode</li>
    <li>Updated: plugin language files</li>
    <li>Fixed: the link for applying the gift card to the cart directly from the cart redirect to wrong page</li>
    <li>Fixed: notice not shown if WooCommerce was not installed</li>
</ul>
<h4>Version 1.4.11 - Released: Nov 03, 2016 </h4>
<ul>
    <li>Fixed: when using the 'gift this product' feature, the amount is not correctly set.</li>
</ul>
<h4>Version 1.4.10 - Released: Nov 02, 2016 </h4>
<ul>
    <li>Added: gift card amounts managed through the accounting.js script</li>
	<li>Updated: gift card product page layout, removing default colors and style to let the page being rendered by the theme</li>
	<li>Fixed: gift card amount set to 0 when in manual mode only</li>
</ul>
<h4>Version 1.4.9 - Released: Oct 20, 2016 </h4>
<ul>
    <li>Fixed: backward compatibility with WooCommerce version 2.6.0 or sooner: wrong product title shown when a gift card is added to the cart</li>
</ul>
<h4>Version 1.4.8 - Released: Oct 11, 2016 </h4>
<ul>
    <li>Updated: removed duplicated 'Add to cart' button</li>
    <li>Updated: the amounts dropdown is now selected on first item by default</li>
    <li>Updated: the preview layout on single product page</li>
    <li>Fixed: empty product title shown after adding a gift card to cart</li>
    <li>Added: spanish translation files</li>
</ul>
<h4>Version 1.4.7 - Released: July 27, 2016 </h4>
<ul>
    <li>Updated: Aelia Currency Switcher compatibility to latest plugin version</li>
	<li>Added: multiple BCC recipients for gift cards sold</li>
	<li>Added: template for gift card footer</li>
	<li>Added: template for gift card suggestion section</li>
</ul>
<h4>Version 1.4.6 - Released: July 12, 2016 </h4>
<ul>
    <li>Fixed: manual amount in physical product do not work</li>
	<li>Fixed: total not updated correctly in mini-cart</li>
</ul>
<h4>Version 1.4.5 - Released: July 05, 2016 </h4>
<ul>
    <li>Fixed: tab 'general' not visible in gift cards products after the update to WooCommerce 2.6.2</li>
</ul>
<h4>Version 1.4.4 - Released: July 01, 2016 </h4>
<ul>
    <li>Fixed: the amount shown on mini cart was not converted in current currency when using the Aelia Currency Switcher plugin</li>
</ul>
<h4>Version 1.4.3 - Released: Jun 30, 2016 </h4>
<ul>
    <li>Fixed: mini cart amounts not updated when a gift card is added to the cart</li>
</ul>
<h4>Version 1.4.2 - Released: Jun 28, 2016 </h4>
<ul>
    <li>Fixed: email footer and header not shown when using the "send now" feature</li>
</ul>
<h4>Version 1.4.1 - Released: Jun 27, 2016 </h4>
<ul>
    <li>Updated: do not show the amount dropdown if only manual amount is enabled</li>
</ul>
<h4>Version 1.4.0 - Released: Jun 14, 2016 </h4>
<ul>
    <li>Added: WooCommerce 2.6 ready</li>
    <li>Added: set the gift cards product as downloadable to let the payment gateway to set the order as completed when paid</li>
    <li>Fixed: issue that would prevent to edit a gift card when the order was in processing status</li>
    <li>Fixed: a warning was shown in product of type other than the gift cards due to a conflict with YITH Dynamic Pricing</li>
    <li>Fixed: wrong gift card object retrieved then using a numeric gift card code</li>
</ul>
<h4>Version 1.3.8 - Released: May 18, 2016 </h4>
<ul>
    <li>Updated: the form-gift-cards.php template file</li>
	<li>Fixed: the discount code was not applied correctly clicking on the email received</li>
</ul>
<h4>Version 1.3.7 - Released: May 09, 2016 </h4>
<ul>
    <li>Added: allow manual entered amounts for physical products</li>
    <li>Added: support to WPML Multiple Currency</li>
    <li>Added: let the vendor to manage his own gift cards when YITH Multi Vendor is active</li>
    <li>Added: gift card code fields could be removed from checkout page via a filter</li>
</ul>
<h4>Version 1.3.6 - Released: May 05, 2016 </h4>
<ul>
    <li>Fixed: gift cards generated twice when used within YITH Multi Vendor plugin</li>
</ul>
<h4>Version 1.3.5 - Released: Apr 29, 2016 </h4>
<ul>
    <li>Added: support to WooCommerce 2.6.0 for edit product page</li>
    <li>Fixed: out of date get_status() function call removed from the /templates/myaccount/my-giftcards.php file</li>
    <li>Fixed: conflict on emails sent by the YITH WooCommerce Points and Rewards plugin</li>
</ul>
<h4>Version 1.3.4 - Released: Apr 26, 2016 </h4>
<ul>
    <li>Fixed: the 'alt' and 'title' attribute of the gift cards template were not localizable</li>
    <li>Updated: yith-woocommerce-gift-cards.pot file</li>
</ul>
<h4>Version 1.3.3 - Released: Apr 21, 2016 </h4>
<ul>
    <li>Fixed: the custom image chosen while purchasing a gift card was not used in the email</li>
    <li>Fixed: resetting the custom image from the edit product page, the featured image was not used anymore</li>
</ul>
<h4>Version 1.3.2 - Released: Apr 13, 2016 </h4>
<ul>
    <li>Fixed: customize gift card button not shown if 'show template' is set to false</li>
</ul>
<h4>Version 1.3.1 - Released: Apr 12, 2016 </h4>
<ul>
    <li>Updated: pre-printed gift cards are not sent automatically when the code is filled</li>
    <li>Fixed: gallery items do not load properly</li>
    <li>Fixed: option for show the shop logo on the gift card template not visible on plugin settings</li>
</ul>
<h4>Version 1.3.0 - Released: Apr 11, 2016 </h4>
<ul>
    <li>Added: create a gallery of standard design from which the customer can choose the one that best fits the festivity or recurrence for which the gift card is being purchased</li>
    <li>Added: new feature for shops selling pre-printed physical gift cards, you can add the code manually instead of being auto generated</li>
    <li>Added: new option let you use the product featured image can be used as the gift card header image</li>
    <li>Added: from the product edit page you can set any image from the media gallery as the gift card header image</li>
    <li>Added: new option let you choose if shop logo should be shown on the gift card template</li>
    <li>Added: you can choose between two layouts for the gift card template</li>
    <li>Fixed: email header not visible when using the bulk action "Order actions" from the order page</li>
</ul>
<h4>Version 1.2.12 - Released: Mar 22, 2016 </h4>
<ul>
    <li>Fixed: gift cards table filter fails after "send now" button pressed</li>
    <li>Fixed: Aelia Currency Switcher add-on, wrong currency shown on emails</li>
    <li>Fixed: unwanted edit link shown on gift cards email</li>
    <li>Fixed: standard coupon not accepted when used together with a gift card</li>
</ul>
<h4>Version 1.2.11 - Released: Mar 15, 2016 </h4>
<ul>
    <li>Fixed: wrong gift card value shown on gift this product for variable product</li>
</ul>
<h4>Version 1.2.10 - Released: Mar 14, 2016 </h4>
<ul>
    <li>Updated: on back end gift cards table page, show the sum of order totals instead of subtotals</li>
    <li>Fixed: duplicated orders shown on back end gift cards table page</li>
</ul>
<h4>Version 1.2.9 - Released: Mar 11, 2016 </h4>
<ul>
    <li>Added: new gift card status: "Dismissed" is for gift card not valid and no more usable.</li>
    <li>Added: Syncronization between gift cards status and order status</li>
    <li>Fixed: wrong calculation on gift card when a manual amount is entered</li>
    <li>Updated: YITH Plugin FW</li>
</ul>
<h4>Version 1.2.8 - Released: Mar 09, 2016 </h4>
<ul>
    <li>Added: Rich snippets for the gift card product</li>
    <li>Added: automatic cart discount clicking from the email received</li>
    <li>Deleted: yith-status-options.php file no more used</li>
</ul>
<h4>Version 1.2.7 - Released: Mar 07, 2016 </h4>
<ul>
    <li>Fixed: ywgc-frontend.min.js not updated to the latest version</li>
    <li>Added: let the customer to change the recipient of gift card, crating a new gift card with update balance</li>
    <li>Added: in my-account page show the order where a gift card was used</li>
    <li>Updated: yith-woocommerce-gift-cards.pot in /languages folder</li>
</ul>
<h4>Version 1.2.6 - Released: Mar 01, 2016 </h4>
<ul>
    <li>Updated: all the gift cards used by a customer are now shown on my-account page</li>
    <li>Added: Aelia MultiCurrency compatibility let you use gift cards in multiple currency environment</li>
</ul>
<h4>Version 1.2.5 - Released: Feb 26, 2016 </h4>
<ul>
    <li>Added: gift cards can be set as disabled and no discount will be applied</li>
    <li>Added: template myaccount/my-giftcards.php for showing gift cards balance</li>
    <li>Added: show balance of used gift cards in my-account page</li>
    <li>Fixed: coupon code section shown twice on cart page based on the theme used</li>
    <li>Updated: removed filter yith_woocommerce_gift_cards_empty_price_html</li>
</ul>
<h4>Version 1.2.4 - Released: Feb 11, 2016 </h4>
<ul>
    <li>Fixed: in cart page the "Coupon" text was not localizable.</li>
    <li>Fixed: the class selector for datepicker conflict with other datepicker in the page</li>
    <li>Fixed: adding to cart of product with "sold individually" flag setted fails</li>
    <li>Fixed: require_once of class.ywgc-product-gift-card.php lead sometimes to "the Class 'WC_Product' not found" fatal error</li>
    <li>Added: compatibility with the YITH WooCommerce Points and Rewards plugin</li>
</ul>
<h4>Version 1.2.3 - Released: Jan 18, 2016 </h4>
<ul>
    <li>Fixed: notification email on gift card code used not delivered to the customer</li>
</ul>
<h4>Version 1.2.2 - Released: Jan 15, 2016 </h4>
<ul>
    <li>Added: compatibility with YITH WooCommerce Dynamic Pricing</li>
</ul>
<h4>Version 1.2.1 - Released: Jan 14, 2016 </h4>
<ul>
    <li>Fixed: missing parameter 2 on emails</li>
</ul>
<h4>Version 1.2.0 - Released: Jan 13, 2016 </h4>
<ul>
    <li>Updated: gift card code is generated only one time, even if the order status changes to 'completed' several time</li>
    <li>Updated: plugin ready for WooCommerce 2.5</li>
    <li>Updated: removed action ywgc_gift_cards_email_footer for woocommerce_email_footer on email template</li>
    <li>Fixed: prevent gift card message containing HTML or scripts to be rendered</li>
    <li>Added: resend gift card email on the resend order emails dropdown on order page</li>
</ul>
<h4>Version 1.1.6 - Released: Dec 29, 2015 </h4>
<ul>
    <li>Added: digital gift cards content shown on gift cards table in admin dashboard</li>
    <li>Added: option to force gift card code sending when automatic sending fails</li>
</ul>
<h4>Version 1.1.5 - Released: Dec 14, 2015 </h4>
<ul>
    <li>Fixed: YITH Plugin Framework breaks updates on WordPress multisite</li>
</ul>
<h4>Version 1.1.4 - Released: Dec 11, 2015 </h4>
<ul>
    <li>Fixed: manual entered text not used in emails</li>
</ul>
<h4>Version 1.1.3 - Released: Dec 08, 2015 </h4>
<ul>
    <li>Fixed: YIT panel script not enqueued in admin</li>
</ul>
<h4>Version 1.1.2 - Released: Dec 07, 2015 </h4>
<ul>
    <li>Fixed: temporary gift card tax calculation</li>
    <li>Updated: temporary gift card is visible on dashboard so it can be set the title and the image</li>
</ul>
<h4>Version 1.1.1 - Released: Nov 30, 2015 </h4>
<ul>
    <li>Fixed: Emogrifier warning caused by typo in CSS</li>
    <li>Fixed: problem that prevent the gift card email from being sent</li>
    <li>Fixed: ask for a valid date when postdated delivery is checked</li>
</ul>
<h4>Version 1.1.0 - Released: Nov 26, 2015 </h4>
<ul>
    <li>Added: optionally redirect to cart after a gift cards is added to cart</li>
    <li>Fixed: postdated gift cards was sent on wrong date</li>
</ul>
<h4>Version 1.0.9 - Released: Nov 25, 2015 </h4>
<ul>
    <li>Fixed: missing function on YIT Plugin Framework</li>
    <li>Updated: gift cards sender and recipient details added on emails</li>
</ul>
<h4>Version 1.0.8 - Released: Nov 24, 2015 </h4>
<ul>
    <li>Fixed: wrong gift card values generated when in WooCommerce Tax Options, prices are set as entered without tax and displayd inclusiding taxes</li>
</ul>
<h4>Version 1.0.7 - Released: Nov 20, 2015 </h4>
<ul>
    <li>Updated: gift card price support price including or excluding taxes</li>
</ul>
<h4>Version 1.0.6 - Released: Nov 19, 2015 </h4>
<ul>
    <li>Updated: Gift Cards object cast to array for third party compatibility</li>
</ul>
<h4>Version 1.0.5 - Released: Nov 17, 2015 </h4>
<ul>
    <li>Fixed: tax not deducted when gift card code was used</li>
</ul>
<h4>Version 1.0.4 - Released: Nov 13, 2015 </h4>
<ul>
    <li>Fixed: multiple gift cards code not generated</li>
</ul>
<h4>Version 1.0.3 - Released: Nov 12, 2015 </h4>
<ul>
    <li>Added: tax class on gift card product type</li>
    <li>Updated: changed action used for YITH Plugin FW loading</li>
    <li>Updated: gift card full amount(product price plus taxes) used for cart discount</li>
</ul>
<h4>Version 1.0.2 - Released: Nov 06, 2015 </h4>
<ul>
    <li>Fixed: coupon conflicts at checkout</li>
</ul>
<h4>Version 1.0.1 - Released: Oct 29, 2015 </h4>
<ul>
    <li>Update: YITH plugin framework</li>
</ul>
<h4>Version 1.0.0 - Released: Oct 22, 2015</h4>
<ul>
    <li>Initial Release</li>
</ul>

    ";}";}