��[<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:28424;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2016-05-25 14:11:30";s:13:"post_date_gmt";s:19:"2016-05-25 14:11:30";s:12:"post_content";s:21724:"[vc_row type="in_container" full_screen_row_position="middle" scene_position="center" text_color="dark" text_align="left" overlay_strength="0.3"][vc_column column_padding="no-extra-padding" column_padding_position="all" background_color_opacity="1" background_hover_color_opacity="1" width="1/1" tablet_text_alignment="default" phone_text_alignment="default"][vc_column_text]Welcome to the Thank God I’m Natural Store, www.tginstore.com (the "Site") owned and operated by Thank God I’m Natural and its affiliates ("TGIN"). Except as otherwise noted herein, these terms and conditions (the “Terms") govern your use of the Site. Please read these Terms carefully, because your use of the Site constitutes your agreement to follow and be bound by these Terms. If you do not agree to these Terms, you should not access or use the Site.

TGIN reserves the right to make changes to the Site and to these Terms from time to time without prior notice to you (including after you have submitted your order). When we make changes, we will post them here. For this reason, we encourage you to review these Terms whenever you use the Site because by visiting the Site, you agree to accept any such changes.

TGIN provides you with access to and use of the Site subject to your compliance with the Terms. No material from the Site may be copied, reproduced, republished, uploaded, posted, transmitted or distributed in any way, except as specifically permitted on the Site. The Site, including all of its information and contents, such as text, data, wallpaper, icons, characters, artwork, images, photographs, graphics, music, sound, messages, graphics, software and the HTML used to generate the pages (collectively, "Materials and Content"), is TGIN property or that of our suppliers or licensors and is protected by patent, trademark and/or copyright under United States and/or foreign laws. Except as otherwise provided on the site or in these Terms, you may not use, download, upload, copy, print, display, perform, reproduce, publish, modify, delete, add to, license, post, transmit, or distribute any Materials and Content from this Site in whole or in part, for any public or commercial purpose without the specific prior written permission of TGIN.

We grant you a personal, limited, non-exclusive, non-transferable license to access the Site and to use the information and services contained here. We reserve the right, for any reason or for no reason, in our sole discretion and without notice to you, to revise the products and services described on the Site and to terminate, change, suspend or discontinue any aspect of the Site, including, but not limited to, the Materials and Content on the Site as well as features and/or hours of availability of the Site, and we will not be liable to you or to any third party for doing so. We may also impose rules for and limits on your use of the Site or restrict your access to part, or all, of the Site without notice or penalty. We have the right to change these rules and/or limitations at any time, in our sole discretion.
<h1>Prohibited Uses</h1>
The Site may be used only for lawful purposes and is available only for your personal, noncommercial use, which shall be limited to viewing the Site, purchasing products, providing information to the Site, and downloading product information for your personal review. You are responsible for your own communications, including the transmission, posting, and uploading of information and are responsible for the consequences of such communications to the Site. TGIN specifically prohibits any use of the Site, and requires all users to agree not to use the Site, for any of the following:
<ul>
 	<li>Posting any information which is incomplete, false, inaccurate or not your own</li>
 	<li>Engaging in conduct that would constitute a criminal offense, giving rise to civil liability or otherwise violate any city, state, national or international law or regulation that would fail to comply with accepted Internet protocol</li>
 	<li>Communicating, transmitting, or posting material that is copyrighted or otherwise owned by a third party unless you are the copyright owner or have the permission of the owner to post it</li>
 	<li>Communicating, transmitting, or posting material that reveals trade secrets, unless you own them or have the permission of the owner</li>
 	<li>Communicating, transmitting, or posting material that infringes on any other intellectual property, privacy or publicity right of another</li>
 	<li>Communicating, transmitting, or transferring (by any means) information or software derived from the site to foreign countries or certain foreign nations in violation of US export control laws</li>
 	<li>Attempting to interfere in any way with the Site's or TGIN’s networks or network security, or attempting to use the Site's service to gain unauthorized access to any other computer system</li>
</ul>
<h1>Limits on Purchases</h1>
TGIN reserves the right to limit or prohibit orders that, in our sole judgment, appear to be placed by dealers, resellers or distributors. TGIN also reserves the right to cease doing business with those customers violating these Terms.
<h1>Security Rules</h1>
Violations of system or network security may result in civil or criminal liability. TGIN will investigate occurrences and may involve, and cooperate with, law enforcement authorities in prosecuting any user or users who are involved in such violations. You are prohibited from violating or attempting to violate the security of the Site, including, without limitation, the following:
<ul>
 	<li>Accessing data not intended for you or logging into a TGIN server or account, which you are not authorized to access</li>
 	<li>Attempting to probe, scan or test the vulnerability of a system or network or to breach security or authentication measures without proper authorization (or succeeding in such an attempt)</li>
 	<li>Attempting to interfere or interfering with the operation of our Site, our provision of services to any other visitors to our Site, our hosting provider or our network, including, without limitation, via means of submitting a virus to the Site, overloading, "flooding", "mailbombing" or "crashing" the Site</li>
 	<li>Forging any TCP/IP packet header or any part of the header information in any e-mail or transmission or posting to our Site</li>
</ul>
<h1>Product and Pricing Information</h1>
Although TGIN has made every effort to display our products and their colors as accurately as possible, the displayed colors of the products will depend upon the monitor of the user, and TGIN cannot guarantee that the user's monitor will accurately portray the actual colors of the products. Products displayed may be out-of-stock or discontinued, and prices are subject to change.

Occasionally there may be information on our Site that contains typographical errors, inaccuracies, or omissions that may relate to product descriptions, pricing, promotions, offers, and availability. We reserve the right to correct any errors, inaccuracies or omissions and to change or update information or cancel orders if any information on the Sites is inaccurate at any time without prior notice (including after you have submitted your order).
<h1>Proprietary Rights</h1>
As between you and TGIN, TGIN is the owner and/or authorized user of any registered or unregistered trademark, trade name and/or service mark appearing on the Site, and is the copyright owner or licensor of the Materials and Content on the Site, unless otherwise indicated. The TGIN logos, designs, titles, phrases and product names and the copyrights, trademarks, service marks, trade dress and/or other intellectual property in such materials (collectively, "TGIN Intellectual Property") are owned by TGIN and may be registered in the United States and internationally. You agree not to display or use the TGIN Intellectual Property in any manner without TGIN's prior written consent. Nothing on the Site should be construed to grant any license or right to use any TGIN Intellectual Property without the prior written consent of TGIN. The information on the Site including, without limitation, all site design, text, graphics, interfaces, and the selection and arrangements is protected by law including, but not limited to, copyright law.

If you make use of the Site, other than as provided herein, in doing so you may violate copyright and other laws of the United States and/or other countries, as well as applicable state laws, and you may be subject to liability for such unauthorized use.
<h1>Personal Information Submitted Through the Sites</h1>
Your submission of personal information through the Site is governed by our Privacy Policy, which is incorporated into these Terms by reference.
<h1>Indemnity</h1>
You agree to defend, indemnify and hold TGIN, its directors, officers, employees, agents and affiliates harmless from any and all claims, liabilities, damages, costs and expenses, including reasonable attorneys' fees, in any way arising from, related to or in connection with your use of the Site, your violation of the Terms or the posting or transmission of any materials on or through the Site by you, including, but not limited to, any third party claim that any information or materials you provide infringes any third party proprietary right.
<h1>Limitation of Liability</h1>
You assume all responsibility and risk with respect to your use of the Site, which is provided "AS IS." TGIN DISLCAIMS ALL WARRANTIES, REPRESENTATIONS AND ENDORSEMENTS OF ANY KIND, EITHER EXPRESS OR IMPLIED, WITH REGARD TO INFORMATION ACCESSED FROM OR VIA THE SITE, INCLUDING WITHOUT LIMITATION, ALL CONTENT AND MATERIALS, FUNCTIONS AND SERVICES PROVIDED ON THE SITE, WHICH ARE PROVIDED WITHOUT WARRANTY OF ANY KIND, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES CONCERNING THE AVAILABILITY, ACCURACY, COMPLETENESS, USEFULNESS, OR CONTENT OF INFORMATION, UNINTERRUPTED ACCESS, AND ANY WARRANTIES OF TITLE, NON-INFRINGEMENT, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. TGIN DOES NOT WARRANT THAT THE SITE OR ITS FUNCTION OR THE CONTENT AND MATERIALS OR THE SERVICES MADE AVAILABLE THEREBY WILL BE TIMELY, SECURE, UNINTERRUPTED OR ERROR FREE, OR THAT DEFECTS WILL BE CORRECTED. TGIN MAKES NO WARRANTY THAT THE SITE WILL MEET USERS' EXPECTATIONS OR REQUIREMENTS. NO ADVICE, RESULTS OR INFORMATION, OR MATERIALS WHETHER ORAL OR WRITTEN, OBTAINED BY YOU THROUGH THE SITE SHALL CREATE ANY WARRANTY NOT EXPRESSLY MADE HEREIN. IF YOU ARE DISSATISFIED WITH THE SITE, YOUR SOLE REMEDY IS TO DISCONTINUE USING THE SITE. ANY MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF THE SITE IS DONE AT YOUR OWN DISCRETION AND RISK AND THAT YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE THAT RESULTS FROM THE DOWNLOAD OF ANY SUCH MATERIAL.

TGIN makes no warranties of any kind regarding any non-TGIN sites to which you may be directed or hyperlinked from this Site. Hyperlinks are included solely for your convenience, and TGIN makes no representations or warranties with regard to the accuracy, availability, suitability or safety of information provided in such non- TGIN sites. TGIN does not endorse, warrant or guarantee any products or services offered or provided by or on behalf of third parties on the Site.

IN NO EVENT SHALL TGIN, ITS AFFILIATES OR ANY OF THEIR RESPECTIVE DIRECTORS, OFFICERS, EMPLOYEES, AGENTS, OR CONTENT OR SERVICE PROVIDERS BE LIABLE TO YOU FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL, EXEMPLARY OR PUNITIVE DAMAGES, LOSSES OR CAUSES OF ACTION (WHETHER IN CONTRACT OR TORT, INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE OR OTHERWISE) ARISING FROM OR IN ANY WAY RELATED TO THE USE OF, OR THE INABILITY TO USE, OR THE PERFORMANCE OF THE SITE OR THE CONTENT AND MATERIALS OR FUNCTIONALITY ON OR ACCESSED THROUGH THE SITE, INCLUDING, WITHOUT LIMITATION, LOSS OF REVENUE, OR ANTICIPATED PROFITS, OR LOST BUSINESS, DATA OR SALES OR ANY OTHER TYPE OF DAMAGE, TANGIBLE OR INTANGIBLE IN NATURE, EVEN IF TGIN OR ITS REPRESENTATIVE OR SUCH INDIVIDUAL HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. <strong>TGIN’s ENTIRE LIABILITY FOR DAMAGES HEREUNDER SHALL IN NO EVENT EXCEED FIFTY DOLLARS (U.S. $50.00).</strong>
<h1>General</h1>
These Terms constitute the entire agreement between you and TGIN and govern your use of the Site, and they supersede any prior agreements between you and TGIN. You also may be subject to additional terms and conditions that are applicable to certain parts of the Site. TGIN may deny you access to the Site at any time, immediately and without notice, if in TGIN 's sole discretion you fail to comply with any provision of these Terms.

You agree that no joint venture, partnership, employment, or agency relationship exists between TGIN and you as a result of your use of the Site.

Any claim or cause of action you may have with respect to TGIN or the Site must be commenced within one (1) year after the claim or cause of action arose.

The failure of TGIN to exercise or enforce any right or provision of these terms shall not constitute a waiver of such right or provision. The invalidity of any term, condition or provision in these terms shall not affect the enforceability of those portions of the terms deemed enforceable by applicable courts of law.

These terms shall be governed by, construed and enforced in accordance with the laws of the State of Illinois, without giving effect to any conflict of law provisions. Any dispute arising under these Terms shall be resolved exclusively by an appropriate federal or state court sitting in the State of Illinois, in Cook County.

You may not assign the Terms or any of your rights or obligations under the Terms without TGIN’s express written consent. The Terms inure to the benefit of TGIN's successors, assigns and licensees. The section titles in these Terms are for convenience only and have no legal or contractual effect.
<h2>CONTACT US</h2>
To contact us with any questions or concerns in connection with these Terms or the Site, or to provide any notice under these Terms to us please refer to our Contact Us page.

<strong>Copyright and Trademark Notice</strong> by
Unless otherwise specified, all materials appearing on this site, including the text, site design, logos, graphics, icons, and images, as well as the selection, assembly and arrangement thereof, are the sole property of this Store., Copyright © 2006, ALL RIGHTS RESERVED.  You may use the content of this site only for the purpose of shopping on this site or placing an order on this site and for no other purpose. No materials from this site may be copied, reproduced, modified, republished, uploaded, posted, transmitted, or distributed in any form or by any means without our prior written permission. All rights not expressly granted herein are reserved. Any unauthorized use of the materials appearing on this site may violate copyright, trademark and other applicable laws and could result in criminal or civil penalties.

<hr />

<strong>Credit Cards</strong> by
We accept the following credit cards: Visa, MasterCard,  and Discover. There is no surcharge for using your credit card to make purchases. Please be sure to provide your exact billing address and telephone number (i.e. the address and phone number your credit card bank has on file for you). Incorrect information will cause a delay in processing your order. Your credit card will be billed upon shipment of your order.

<hr />

<strong>Links</strong> by
This site may contain links to other sites on the Internet that are owned and operated by third parties. You acknowledge that we're not responsible for the operation of or content located on or through any such site.

<hr />

<strong>Money Orders, Cashier’s Checks, Company Checks, &amp; Personal Checks</strong> by
We accept money orders, cashier’s checks, personal checks, and company checks in U.S. Dollars only. Orders are processed upon receipt of a money order or cashier’s check. For personal and company checks, please allow up to 10 banking days after receipt for clearance of funds before the order is processed. We cannot guarantee the availability of a product by the time funds clear or payment is received. We will charge a $25 fee on all returned checks.

<hr />

<strong>Multiple Product Orders</strong> by
For a multiple product order, we will make every attempt to ship all products contained in the order at the same time. Products that are unavailable at the time of shipping will be shipped as they become available, unless you inform us otherwise. You will only be charged for products contained in a given shipment, plus any applicable shipping charges. You will only be charged for shipping at the rate quoted to you on your purchase receipt. The entirety of this shipping charge may be applied to the first product(s) shipped on a multiple shipment order.

<hr />

<strong>Order Acceptance Policy</strong> by
Your receipt of an electronic or other form of order confirmation does not signify our acceptance of your order, nor does it constitute confirmation of our offer to sell. Sample Store reserves the right at any time after receipt of your order to accept or decline your order for any reason or to supply less than the quantity you ordered of any item.

<hr />

<strong>Other Conditions</strong> by
These Conditions will supersede any terms and/or conditions you include with any purchase order, regardless of whether Sample Store. signs them or not. We reserve the right to make changes to this site and these Conditions at any time.

<hr />

<strong>Out-of-Stock Products</strong> by
We will ship your product as it becomes available. Usually, products ship within 24-48 hours the next business day.  However, there may be times when the product you have ordered is out-of-stock which will delay fulfilling your order. We will keep you informed of any products that you have ordered that are out-of-stock and unavailable for immediate shipment. You may cancel your order at any time prior to shipping.

<hr />

<strong>Privacy</strong> by
We keep your personal information private and secure. When you make a purchase from our site, you provide your name, email address, credit card information, address, phone number, and a password. We use this information to process your orders, to keep you updated on your orders and to personalize your shopping experience.Our secure servers protect your information using advanced encryption techniques and firewall technology.

To keep you informed about our latest offers, we may notify you of current promotions, specials and new additions to the Sample Store site. You may unsubscribe from our newsletters by following the unsubscribe instructions in any email you receive from us.

When entering any of our contests or prize drawings, you provide your name, email address and mailing address. If you win, we will send the prize to the address entered and notify you by email. When you enter a contest or drawing you are also included in our newsletter list to receive notice of promotions, specials and new additions to the Sample Store site. You may unsubscribe from this news list by following the unsubscribe instructions in any email received.

We use "cookies" to keep track of your current shopping session to personalize your experience and so that you may retrieve your shopping cart at any time.

<hr />

<strong>Privacy on Other Web Sites</strong> by
Other sites accessible through our site have their own privacy policies and data collection practices. Please consult each site's privacy policy. Our Store is not responsible for the actions of third parties.

<hr />

<strong>Returns</strong> by
We will gladly accept the return of products that are defective due to defects in manufacturing and/or workmanship for 30 days from the date of purchase. Fulfillment mistakes that we make resulting in the shipment of incorrect product to you will also be accepted for return 30 days from the date of purchase.

<hr />

<strong>Shipping Policy</strong> by
Shipping Time -- Most orders received before 5:00PM will ship the same day, provided the product ordered is in stock. Most orders received after 5:00 PM will ship the next business day. Orders are not processed or shipped on Saturday or Sunday, except by prior arrangement.We cannot guarantee when an order will arrive. Consider any shipping or transit time offered to you by Sample Store or other parties only as an estimate. We encourage you to order in a timely fashion to avoid delays caused by shipping or product availability.

<hr />

<strong>Taxes</strong> by
Our Store shall automatically charge and withhold the applicable sales tax for orders to be delivered to addresses within the same state. For orders shipped to other states, you are solely responsible for all sales taxes or other taxes.

<hr />

<strong>Typographical Errors</strong> by
In the event a product is listed at an incorrect price due to typographical error or error in pricing information received from our suppliers, Our Store shall have the right to refuse or cancel any orders placed for product listed at the incorrect price. Our Store shall have the right to refuse or cancel any such orders whether or not the order has been confirmed and your credit card charged. If your credit card has already been charged for the purchase and your order is canceled, Our Store shall immediately issue a credit to your credit card account in the amount of the incorrect price[/vc_column_text][/vc_column][/vc_row]";s:10:"post_title";s:17:"Terms & Conditons";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:15:"terms-conditons";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-11-21 13:43:09";s:17:"post_modified_gmt";s:19:"2018-11-21 13:43:09";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:48:"http://thankgodimnatural.com/tgin/?page_id=28424";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}