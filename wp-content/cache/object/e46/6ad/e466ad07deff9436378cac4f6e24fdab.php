��[<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:125414;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2018-11-20 05:01:47";s:13:"post_date_gmt";s:19:"2018-11-20 05:01:47";s:12:"post_content";s:2268:"[vc_row][vc_column][vc_column_text]
<div class="text-head">Our Founder</div>
[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column width="1/2"][vc_single_image image="105113" img_size="full"][/vc_column][vc_column width="1/2"][vc_column_text]Chris-Tia Donaldson is the Founder and Chief Executive Officer of Thank God It’s Natural (tgin), a manufacturer of natural hair and skin care products, currently sold in Walmart, Target, Whole Foods, Sally’s Beauty, and Walgreen’s. In her role, she oversees all aspects of day-to-day operations, sales, and strategic partnerships.

Chris-Tia has been featured in major media publications such as USA Today, Marie Claire, Essence, Black Enterprise, Ebony, Heart &amp; Soul, and the Chicago Tribune. Her book Thank God I’m Natural: The Ultimate Guide to Caring for Natural Hair is a #1 Amazon bestseller, and was hailed the “Natural Hair Bible” by Essence Magazine.

Prior to starting her own company, Chris-Tia represented Fortune 500 companies in complex business transactions involving technology and open source code. Chris-Tia earned her A.B. in Economics from Harvard University with high honors, and is a graduate of Harvard Law School.

In 2015, Chris-Tia was diagnosed with breast cancer. During her treatment, she learned that having money could make the difference between living and dying when it came to treating this conditioner. In her observation, few organizations existed that provided support and social services to help women with transportation, child care, parking, or seeking disability leave from their place of employment.

Today, she uses her success in the beauty space to advocate for women experiencing financial difficulties, who are undergoing treatment, to highlight health disparities due to race and socio-economic factors, and to empower women to listen to their bodies through the tgin Foundation.

Chris-Tia is currently penning her next book, which she plans to release in the summer of 2018, called This is Only a Test: What Breast Cancer Taught me about Faith, Love, Hair and Business.

For updates about Chris-Tia, follow her on:

Facebook (www.facebook.com/thankgodimnatural)
Twitter (@tginceo)
Instagram (@tginceo)[/vc_column_text][/vc_column][/vc_row]";s:10:"post_title";s:4:"Test";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:6:"test-2";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-11-20 05:05:11";s:17:"post_modified_gmt";s:19:"2018-11-20 05:05:11";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:52:"http://import.thankgoditsnatural.com/?page_id=125414";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}