��[<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:35739;s:11:"post_author";s:1:"3";s:9:"post_date";s:19:"2018-02-03 06:46:22";s:13:"post_date_gmt";s:19:"2018-02-03 12:46:22";s:12:"post_content";s:6913:"[vc_row css=".vc_custom_1517662953093{margin-top: 60px !important;}"][vc_column][vc_column_text css=".vc_custom_1517663525486{padding-bottom: 30px !important;}"]
<h1 style="text-align: center;">Customer FAQs</h1>
[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_custom_heading text="Product Related"][vc_tta_accordion][vc_tta_section title="Are your products 100% natural?" tab_id="1537342750425-c5113547-684d"][vc_column_text]
<div id="ufaq-post-35967" class="ewd-ufaq-post-margin ufaq-faq-post">

We would love for our hair products to be 100% natural, but unfortunately, we have to include ingredients to improve the shelf life of the product and keep it from separating. Also, we have to include a small amount of preservatives to prevent the products from going rancid or developing mold or any kind of bacteria. Note, if our products were completely natural they would have a limited shelf life (7-10 days) and would most likely need to be kept in the refrigerator.

At this time, our products do not contain any parabens, sulfates, artificial colors and there is no animal testing. We value the health and safety of our customer(s) and are constantly looking for ways to improve the quality and efficacy of our products, while maintaining their superior performance.

</div>
[/vc_column_text][/vc_tta_section][vc_tta_section title="Where can I find tgin products?" tab_id="1537342750451-384a5f59-26b1"][vc_column_text]
<div id="ufaq-post-35759" class="ewd-ufaq-post-margin ufaq-faq-post">

TGIN is currently sold at CVS, Target, Sally Beauty, Whole Foods, Walgreen’s, Rite Aid and Mariano’s. Please check out this link for a location nearest you:
<div aria-hidden="false">

<a href="https://thankgoditsnatural.com/store-locator" target="_blank" rel="noopener noreferrer">https://thankgoditsnatural.com/store-locator</a>

</div>
</div>
[/vc_column_text][/vc_tta_section][vc_tta_section title="Do you offer sample sizes?" tab_id="1537430396645-c87c9b47-5ffc"][vc_column_text]
<div id="ufaq-post-35756" class="ewd-ufaq-post-margin ufaq-faq-post">
<div id="ufaq-post-35756" class="ewd-ufaq-post-margin ufaq-faq-post">

We are pleased that your search has led you to tgin products! It is extremely important to us that our customers are pleased with the products that they purchase. That is precisely why our top selling products are offered in sample size! This option was created in part for customers who would like to try our products out for a reasonable price. You can find those products listed directly at this link here:

<a href="/product-category/sample-sizes/">http://import.thankgoditsnatural.com/product-category/sample-sizes/</a>

</div>
</div>
[/vc_column_text][/vc_tta_section][vc_tta_section title="What are your ingredient standards?" tab_id="1537430515812-a258721e-dcb4"][vc_column_text]Our products are made primarily with natural and organic ingredients. We go to great lengths to ensure that are products are free of parabens, phthalates, sulfates, petrolatum, lanolin or artificial colors.[/vc_column_text][/vc_tta_section][vc_tta_section title="Are tgin products tested on animals?" tab_id="1537430553873-4a0bf2b6-1c62"][vc_column_text]No, tgin products are completely cruelty free and are not tested on animals.[/vc_column_text][/vc_tta_section][/vc_tta_accordion][/vc_column][/vc_row][vc_row][vc_column][vc_custom_heading text="Services Related"][vc_tta_accordion][vc_tta_section title="I have a question on my Amazon Order." tab_id="1537430769801-218a2d0b-eea8"][vc_column_text]For orders placed via Amazon.com please reach out to Amazon directly.[/vc_column_text][/vc_tta_section][vc_tta_section title="Can I contact you about my event/request sponsorship/blogger collaboration?" tab_id="1537430806869-b5f8beab-0a21"][vc_column_text]
<div id="ufaq-post-35781" class="ewd-ufaq-post-margin ufaq-faq-post">

For event and/or sponsorship requests or blogger/reviewer collaboration request please contact us at
<div aria-hidden="false"><a href="mailto:sponsorships@tginatural.com" target="_blank" rel="noopener noreferrer">sponsorships@tginatural.com</a></div>
</div>
[/vc_column_text][/vc_tta_section][vc_tta_section title="What is your return policy?" tab_id="1537430842202-d7dff2af-9872"][vc_column_text]
<div id="ufaq-post-35762" class="ewd-ufaq-post-margin ufaq-faq-post">

If you are not satisfied with tgin products you may return any unopened/unused products within 14 days to:

Address:- <strong>Thank God It’s Natural</strong> <em>910 W. Van Buren, Suite 100 #204 Chicago, IL 60607</em>

Upon receipt of the products, we’ll gladly issue you a refund. To ensure there are no issues, we recommend that you get a tracking number for your shipment with delivery confirmation.

</div>
[/vc_column_text][/vc_tta_section][/vc_tta_accordion][/vc_column][/vc_row][vc_row][vc_column][vc_custom_heading text="Shipping Related"][vc_tta_accordion][vc_tta_section title="Do you ship internationally?" tab_id="1537430962919-95be3260-0e12"][vc_column_text]Yes, absolutely! In order to calculate the shipping rate you will be charged at the time of order, please visit the following URL: <a href="https://www.paypal.com/shipnow">https://www.paypal.com/shipnow</a>[/vc_column_text][/vc_tta_section][vc_tta_section title="How do I track my order?" tab_id="1537430996237-a31487a2-4f96"][vc_column_text]When your order is shipped, you will receive an order confirmation from PayPal, along with your tracking number. The United States Postal Service can take up to 3 days to update its tracking information online. Note, there have been instances where our customers have received their products before it is updated online[/vc_column_text][/vc_tta_section][vc_tta_section title="What is my tracking number?" tab_id="1537431023706-3d1b7a5c-5d02"][vc_column_text]When your order is shipped, you will receive an order confirmation from PayPal, along with your tracking number. The United States Postal Service can take up to 3 days to update the tracking information online. Note, there have been instances where our customers have received their products before it is updated online. Please be advised that tgin is not responsible for lost or stolen packages once your items are delivered by the United States Post Office, UPS or FedEx.[/vc_column_text][/vc_tta_section][vc_tta_section title="When will my order ship?" tab_id="1537431051065-d57d9181-7219"][vc_column_text]When your order is received, it takes up to 2 business days to process the order. During the holiday season (which includes Black Friday) this can increase to 3-5 business days. Once your order is processed, it can take anywhere from 3-5 business days for your order to arrive at your destination depending on how far you live from Chicago. Note, we are closed on weekends and holidays.[/vc_column_text][/vc_tta_section][/vc_tta_accordion][/vc_column][/vc_row]";s:10:"post_title";s:14:"Customer FAQ's";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:13:"customer-faqs";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-11-20 09:24:15";s:17:"post_modified_gmt";s:19:"2018-11-20 09:24:15";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:43:"http://thankgodimnatural.com/?page_id=35739";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}