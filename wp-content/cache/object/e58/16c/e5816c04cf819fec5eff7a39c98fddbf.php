��[<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:6:"104971";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2018-08-28 10:35:25";s:13:"post_date_gmt";s:19:"2018-08-28 10:35:25";s:12:"post_content";s:1347:"Create soft touchable curls without the crunch using tgin Curl Bomb Curl Definer Moisturizing Styling Gel. This unique formula aids in moisture retention, while providing long lasting curl definition. No parabens, sulfates, petrolatum, lanolin, artificial colors, or animal testing.

<strong>BENEFITS:</strong>
<ul>
 	<li>Lightweight formula that defines curls and provides soft hold</li>
 	<li>Great for wash-n-go's</li>
 	<li>Non-flaking formula</li>
 	<li>Reduces frizz</li>
 	<li>Enhances shine</li>
 	<li>Great for all hair types</li>
</ul>
<strong>DIRECTIONS</strong>: Wash hair with<strong> tgin Moisture Rich Sulfate Free Shampoo</strong> to cleanse the scalp and remove excess product buildup. Follow with <strong>tgin Triple Moisture Replenishing Conditioner</strong>. Rinse out. Apply a small amount of <strong>tgin Green Tea Super Moist Leave in Conditioner</strong> to wet hair followed by <strong>tgin Curl Bomb</strong> and style as usual.

<strong>INGREDIENTS:</strong> WATER, PVP/VA COPLOYMER, POLYSORBATE 20, MACA ROOT (LEPIDIUM MEYENII), BLACK COHOSH (CIMICIFUGA RACEMOSA), MILK THISTLE (SILYBUM MARIANUM), NETTLE LEAF (URTICA DIOICA), OLEA EUROPAEA (OLIVE) FRUIT OIL, GLYCERIN, CARBOMER, HYDROLYZED WHEAT PROTEIN, AMINOMETHYL PROPANOL, SODIUM HYDROXYMETHYLGLYCINATE, TETRASODIUM EDTA, FRAGRANCE (PARFUM).

&nbsp;";s:10:"post_title";s:42:"Curl Bomb Moisturizing Styling Gel - 13 OZ";s:12:"post_excerpt";s:24:"Natural Hair Styling Gel";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:40:"curl-bomb-moisturizing-styling-gel-13-oz";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-09-07 10:48:21";s:17:"post_modified_gmt";s:19:"2018-09-07 10:48:21";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:60:"http://localhost/tgin/product/import-placeholder-for-105731/";s:10:"menu_order";s:1:"0";s:9:"post_type";s:7:"product";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}