��[<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:28480;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2016-06-20 16:13:55";s:13:"post_date_gmt";s:19:"2016-06-20 16:13:55";s:12:"post_content";s:52900:"[vc_row type="in_container" full_screen_row_position="middle" scene_position="center" text_color="dark" text_align="left" overlay_strength="0.3"][vc_column column_padding="no-extra-padding" column_padding_position="all" background_color_opacity="1" background_hover_color_opacity="1" el_class="sec-heading" tablet_text_alignment="default" phone_text_alignment="default"][vc_column_text]
<h2>our careers</h2>
[/vc_column_text][/vc_column][/vc_row][vc_row type="in_container" full_screen_row_position="middle" scene_position="center" text_color="dark" text_align="left" top_padding="75" overlay_strength="0.3"][vc_column column_padding="no-extra-padding" column_padding_position="all" background_color_opacity="1" background_hover_color_opacity="1" tablet_text_alignment="default" phone_text_alignment="default"][vc_column_text]
<h1><strong>Brand Manager – Chicago Only</strong></h1>
We’ve kicked off the search for a Brand Manager who will lead the development of the brand positioning for tgin. You will lead the life cycle brand vision including product research and ideation, go-to-market launch campaigns, continued vertical growth and development, and ongoing consumer research and analysis. You will be responsible for P&amp;L for the vertical and its current and future portfolio of products. Note, this position is both strategic and tactical.

<strong> </strong><strong>What you’ll do:</strong>
<ul>
 	<li>Exhibit full ownership of new product development from concept to launch in the hair vertical</li>
 	<li>Lead the vision and drive sustainable market share and profitable growth, based on in-depth competitive landscape and consumer insights</li>
 	<li> Lead the innovation strategy and product development for full size, sample size, set and seasonal products</li>
 	<li>Lead the vision for competitive product positioning to continually reinforce point of difference including assortment architecture, pricing and innovation pipeline</li>
 	<li>Develop category and product playbook including communication dossier, advertising guidelines, features and benefits (functional, technical and emotional), reason to believe, education priorities and competitive positioning</li>
 	<li>Lead the product and ingredient storytelling to amplify brand desirability and differentiation vs mass and prestige competition</li>
 	<li>Manage the current business and own the category vertical P&amp;L, leading action planning and go-to-market strategies for new product launches</li>
 	<li>Develop and ensure execution of brand marketing and 360 degree marketing support calendar based on 12-month launch window</li>
 	<li>Define and develop trade marketing and promotional marketing tools including sampling, sets, POS, displays and other consumer elements</li>
 	<li> Coordinate the department's strategy  with respect to events, sponsorship, video, partnerships, campaigns, print/radio and tv.</li>
 	<li>Work closely with Finance and Strategy to analyze tgin’s existing customer base and target audience. Actively and expertly uses this data to target marketing and improve performance.</li>
 	<li>Prepare and oversees departmental budgets.</li>
</ul>
<strong> You’ll love this job if you’re:</strong>
<ul>
 	<li>A critical thinker. Lead project management with impeccable attention to detail</li>
 	<li>Adaptable and ambitious. You will enthusiastically take on other assignments as needed to support your team!</li>
 	<li>Strong leader. Exhibit initiative and sense of urgency to drive to execution against goals, including on time launches and minimal variance vs. budget</li>
</ul>
<strong> What you’ll need:</strong>
<ul>
 	<li>Bachelor’s degree. MBA is a plus.</li>
 	<li>Minimum 3 years experience in a brand management, product/digital marketing or management consulting in the beauty/cosmetics space</li>
 	<li>Proven track record of building intelligent, data-driven, profit generating strategies</li>
 	<li>Proven track record of configuring, concepting and altering consumer product portfolios</li>
 	<li>Expert knowledge of communicating features, benefits and values to consumers</li>
 	<li>Team player with experience working with engineering, marketing, paid media and creative teams in a fast-paced, agile environment</li>
</ul>
<strong>COMPENSATION: </strong>TBD, but negotiable based on experience.

Please email resume,  cover letter and salary requirements to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>[/vc_column_text][vc_column_text]
<h1><strong>Marketing &amp; Product Development Coordinator - Chicago Only</strong></h1>
<h2><strong>Responsibilities:</strong></h2>
<ul>
 	<li>Identify innovation hunting grounds and new product opportunities</li>
 	<li>Contribute to new product ideation including but not limited to concept, packaging, naming, formula, product launch plans and cost of goods sold</li>
 	<li>Draft new product consumer communications and retail sell-in tools</li>
 	<li>Develop and execute new product positioning statements and write new product concepts.</li>
 	<li>Prepare and circulate New Product Sheets, puts together mock-ups of products, fills out Web Sheets, and ensures all appropriate paper work for tgin products are submitted to the appropriate manager.</li>
 	<li>Assess new product innovation performance to identify key learnings that can be applied to future product launches</li>
</ul>
<strong>COMPENSATION: </strong>TBD, but negotiable based on experience.

Please email resume and cover letter to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>[/vc_column_text][vc_column_text]
<h1 style="font-weight: 400;"><strong>tgin Marketing and Events Coordinator -Chicago Only</strong></h1>
<p style="font-weight: 400;">We are looking for a detail-oriented Marketing and Events coordinator to help support tgin’s event marketing programs. Primary focus for this new role is on tactical planning and execution of marketing events and trade shows. The Marketing and Events  Coordinatorassists in creating brand awareness through advertising, social media, digital and more. This role manages projects, campaigns, and budgets. This individual is also responsible for status reports on marketing efforts, brand analysis, competitive information, and metrics. Must be able to communicate effectively across different levels of the organization, and meet deadlines for multiple projects.<strong> </strong></p>
<p style="font-weight: 400;"><strong><u>Job Requirements</u></strong></p>

<ul>
 	<li style="font-weight: 400;">Assisting team members with day-to-day marketing tasks and coordinating marketing projects and activities as requested</li>
 	<li style="font-weight: 400;">Organize the production of branded items such as stationery, merchandise, giveaways for employee and event use</li>
 	<li style="font-weight: 400;">Support the in-house marketing and design team by coordinating and collating content</li>
 	<li style="font-weight: 400;">Set up tracking systems for marketing campaigns via trade shows and events</li>
 	<li style="font-weight: 400;">Assisting with the production of artwork, sourcing images, print buying and checking copy</li>
 	<li style="font-weight: 400;">Analyze/ benchmark competitor sponsorships and events</li>
 	<li style="font-weight: 400;">Work with Marketing Administrative Assistant to schedule, book and coordinate logistics for events, meetings, trade shows</li>
 	<li style="font-weight: 400;">Support full marketing team with lead generation from events, recording of data, metrics, and staying within budget requirements</li>
 	<li style="font-weight: 400;">This position will entail independent travel, including some weekends, hosting of clients, and management of events – travel requirement 25%.  <strong><em>April – August are our busiest months for travel.</em></strong></li>
</ul>
<p style="font-weight: 400;"><span style="text-decoration: underline;"><strong>Qualifications</strong></span></p>

<ul>
 	<li style="font-weight: 400;">Bachelor's degree</li>
 	<li style="font-weight: 400;"><strong>Ability to travel approximately 25%</strong></li>
 	<li style="font-weight: 400;">2-3 years of marketing, sales or customer service experience</li>
 	<li style="font-weight: 400;">Previous trade show or event planning experience is a bonus, but not required</li>
 	<li style="font-weight: 400;">Demonstrate strong competence in simultaneously managing multiple projects with attention to detail, achieving all deadlines, and performing analysis to determine ROI</li>
 	<li style="font-weight: 400;">Self-motivated and independent</li>
 	<li style="font-weight: 400;">Ability to work in a fast paced environment, with multiple and changing priorities while maintaining strong focus on execution and results.</li>
 	<li style="font-weight: 400;">Excellent written and verbal communication skills, including presentation skills</li>
 	<li style="font-weight: 400;">Strong organizational and planning skills</li>
 	<li style="font-weight: 400;">Detail-oriented with excellent follow-up, budgeting, and time management skills</li>
 	<li style="font-weight: 400;">Exceptional customer service and communication skills</li>
 	<li style="font-weight: 400;">Ability to stand for extended periods of time</li>
 	<li style="font-weight: 400;">Able to problem solve effectively</li>
</ul>
COMPENSATION: TBD, but negotiable based on experience.

Please email resume and cover letter to<a href="mailto:tgincareers@gmail.com"> tgincareers@gmail.com</a>[/vc_column_text][vc_column_text]
<h1><strong>Marketing Coordinator </strong></h1>
Thank God It’s Natural (tgin), a manufacturer of natural hair and body products, is seeking a Marketing Coordinator to become a part of our vibrant Marketing division, working on projects focused in the hair, beauty and fashion industries. We are seeking sharp, enthusiastic candidates with a desire to work on creative projects involving graphic design, motion graphics, video and photography in the beauty industry! This position is ideal for someone who is creative and passionate about marketing, and has the ability to execute and strong attention to detail. A strong preference will be given to candidate with an art background or who have a  degree in Graphic Design, Marketing, Art, Photography, Creative Writing or a related field.  <strong>Candidates must live in Chicago!</strong>

<strong>RESPONSIBILITIES: </strong>

- Collaborate with creative design teams to drive impressions, word of mouth buzz, social media follows and bottom- line sales
- Design content for multimedia campaigns including collateral materials, video promotions, web promotions, social media marketing and email communications
- Produce creative for event marketing initiatives including (but not limited to) press events, consumer engagement events, product launches, multi-city tours, etc.
- Assist with creative design for new business presentations and campaign reports

<strong>Examples of the Projects We’re Currently Working On:</strong>
- PR/Press Releases
- <a href="http://www.tginatura.com/">Website Sliders</a>
- <a href="https://www.instagram.com/p/BcqhED-h_lb/?taken-by=tginatural"> </a><a href="https://www.instagram.com/p/BcqhED-h_lb/?taken-by=tginatural">Lifestyle Photos</a>
- <a href="https://www.instagram.com/p/Bc0QThhBd1t/?taken-by=tginatural">Commercial Photo Shoots</a>
- <a href="https://www.instagram.com/p/Bb9o9m1BskE/?taken-by=tginatural">Motion Graphics</a>
- Internal Newsletters
- Billboards
- Campaigns (wash n go, fall season, holiday, twitter chats) - See attached photo

<strong> </strong><strong>KNOWLEDGE, SKILLS &amp; ABILITIES/CREDENTIALS: </strong>
- Creative and/or passionate about consumer marketing
-  Working knowledge of PhotoShop, Illustrator, InDesign, PowerPoint-- a must!
- Amazing social media and copy writing skills are a plus!
- Minimum 1-2 years of experience in related fields is ideal!
- Ability to work well under pressure and meet project deadlines
- Strong preference for individuals with (or pursuing) a BS/BA in Graphic Design, Marketing, Art, Photograpy or a related field

<strong>KEY PERSONALITY TRAITS:</strong>
- Exceptional communication skills
- Exceptional multitasking
- Passion for customer service and complete satisfaction
- Ability to work both independently and remotely
- Ability to take direction and complete assignments on deadline
* Interested applicants should send cover letter, resumes and requested salary to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a> for consideration.[/vc_column_text][vc_column_text]
<h1><strong>Warehouse/Operations Manager – Chicago</strong></h1>
<strong><u>Overview</u></strong>

Join the creative and energetic <strong>Thank God It’s Natural (tgin)</strong> Operations Team, and help us further enhance our strong brand. We are a small natural hair business looking to hire a Warehouse Manager to oversee factory operations. As our Warehouse Manager you must be meticulous and extremely detailed oriented, comfortable working in a fast paced environment and capable of multi-tasking.

<strong><u>Shipping
</u></strong>
<ul>
 	<li>Oversee the Shipping Department</li>
 	<li>Draft and Maintain Shipping Best Practices Document</li>
 	<li>Review orders shipped daily from Amazon and 3dcart customer orders.</li>
 	<li>Create, package, and ship domestic/international wholesale orders.</li>
 	<li>Operate the shopping cart software to retrieve and prepare orders.</li>
 	<li>Schedule pickup times to ship daily orders (USPS) and wholesale orders (Fed Ex).</li>
 	<li>Respond to Amazon.com customer inquiries.</li>
 	<li>Order shipping supplies from USPS &amp; Staples while staying within budget.</li>
 	<li>Verify incoming shipments from UPS &amp; Fed Ex, and the like.</li>
 	<li>Provide services: process returns, exchanges, adjustments and payments.</li>
 	<li>Track and report the status of inventory, orders, and analyze data from multiple sources to effectively problem solve on behalf of customers/retailers.</li>
 	<li>File Claims for lost or stolen packages.</li>
 	<li>Respond toCRMs via 3dshopping cart.</li>
 	<li>Meet with representatives USPS, Fed Ex, UPS to negotiate lower rates.</li>
</ul>
<strong><u>Production</u></strong>
<ul>
 	<li>Review production schedule every Monday and forecast inventory based on schedule.</li>
 	<li>Bottle/label/make conditioners, honey miracle, shea butter, argan oils, sample packs, etc. products on an as needed basis.</li>
 	<li>Oversee daily clean of the production area and office to ensure organization and order.</li>
 	<li>Understand the shelf life of tgin’s natural hair and skin care products.</li>
 	<li>Stock shelves using the FIFO method.</li>
</ul>
<strong><u>Operations
</u></strong>
<ul>
 	<li>Decide which projects the Production/Shipping team should focus on and set an aggressive timeline to reach those goals.</li>
 	<li>Update inventory data in Google Docs on weekly basis.</li>
 	<li>Identify, recommend and implement changes to improve productivity and reduce cost and scrap, monitor scrap and rework data.</li>
 	<li>Direct the establishment, implementation and maintenance of production standards.</li>
 	<li>Direct and coordinate various programs essential to manufacturing procedures (e.g., training, safety, housekeeping, cost reduction, worker involvement, security, etc.).</li>
 	<li>Initiate and coordinate major projects, (e.g., plant layout changes, installation of capital equipment, major repairs, etc.).</li>
 	<li>Develop the manufacturing plan and establish procedures for maintaining high standards of manufacturing operations to ensure that products conform to established customer and company quality standards.</li>
 	<li>Perform miscellaneous duties and projects as assigned and required.</li>
</ul>
<strong>COMPENSATION: </strong>Full-time salary negotiable based on experience.

Please email resume and cover letter to <a href="http://tgincareers@gmail.com">tgincareers@gmail.com</a>[/vc_column_text][vc_column_text]
<h1>Assistant Warehouse Manager - Chicago</h1>
<strong><u>Overview</u></strong>

Join the creative and energetic <strong>Thank God It’s Natural (tgin)</strong> Operations Team, and help us further enhance our strong brand. We are a small natural hair business looking to hire a Warehouse Assistant Manager to oversee factory operations. As our Assistant Manager you must be meticulous and extremely detailed oriented, comfortable working in a fast paced environment and capable of multi-tasking. You will report to the Warehouse manager and help to oversee warehouse processes to ensure smooth flow. You will be responsible for the warehouse when the Warehouse manager is out.

<strong><u>Responsibilities:</u></strong>
<ul>
 	<li>Adhere to the production schedule and directives.</li>
 	<li>Communicate with upper management to meet daily production goals. Your days will involve helping to mix, pack, and label our products to including but not limited to oil and shea butter.</li>
 	<li>Assist with keeping the warehouse stocked with materials, break down boxes for recycling, and clean and organize the warehouse at the end of the day.</li>
 	<li>Cross-training will also be required in our Shipping Department, as you will back them up in times of heavy shipping, such as: holidays, sales, and events, and any special projects that may arise.</li>
 	<li>Shipping tasks include lifting, organizing, assembling and labeling boxes, case-packing product and getting it on the shelf for our wholesale customers.</li>
 	<li>Must be comfortable hauling in large material shipments on pallet jacks.</li>
 	<li>Oversee each department’s daily clean up at the end of each production day to maintain a safe and clean working environment. You will also be responsible for weekly cleanup of our communal kitchen and production areas.</li>
 	<li>Special projects as needed.</li>
</ul>
<strong><u>Abilities: </u></strong>
<ul>
 	<li>The ability to shift gears quickly.</li>
 	<li>Maintains focus, skill, and speed to help the team maintain output goals.</li>
 	<li>You must be self-motivated, careful, attentive, and on task at all times in production.</li>
 	<li>Must have a strong work ethic.</li>
 	<li>Candidate would ideally be familiar with production and shipping duties.</li>
 	<li>You must be able to lift 50 pounds repeatedly.</li>
 	<li>The ability to stand on your feet between 6 to 7 hours per day.</li>
 	<li>As you will be working hands-on with the materials you must be able to tolerate direct contact with fragrance and essential oils.</li>
</ul>
* Interested applicants should send cover letters and resumes to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a> for consideration.[/vc_column_text][vc_column_text]
<h1><strong>Marketing Intern – Creative Projects</strong></h1>
Thank God It’s Natural (tgin), a manufacturer of natural hair and body products, is seeking interns to become a part of our vibrant Marketing division, working on projects focused in the hair, beauty and fashion industries. We are seeking sharp, enthusiastic interns with a desire to work on creative projects involving graphic design, motion graphics, video and photography in the beauty industry! This internship is ideal for college seniors and recent college graduates, who are creative and passionate about marketing. A strong preference will be given to candidate was an art background or are graduating and or pursuing a degree in Graphic Design, Marketing, Art, Photography or a related field.  <strong>Candidates must live in Chicago!</strong>

Required Hours: 10-15/weekly

<strong>RESPONSIBILITIES:</strong>

- Collaborate with creative design teams to drive impressions, word of mouth buzz, social media follows and bottom- line sales

- Design content for multimedia campaigns including collateral materials, video promotions, web promotions, social media marketing and email communications

- Produce creative for event marketing initiatives including (but not limited to) press events, consumer engagement events, product launches, multi-city tours, etc.

- Assist with creative design for new business presentations and campaign reports

<strong>Examples of the Projects We’re Currently Working On:</strong>

-       PR/Press Releases

-       <a href="http://www.tginatura.com/">Website Sliders</a>

-      <a href="https://www.instagram.com/p/BcqhED-h_lb/?taken-by=tginatural"> </a><a href="https://www.instagram.com/p/BcqhED-h_lb/?taken-by=tginatural">Lifestyle Photos</a>

-       <a href="https://www.instagram.com/p/Bc0QThhBd1t/?taken-by=tginatural">Commercial Photo Shoots</a>

-       <a href="https://www.instagram.com/p/Bb9o9m1BskE/?taken-by=tginatural">Motion Graphics</a>

-       Internal Newsletters

-       Billboards

-       Campaigns (wash n go, fall season, holiday, twitter chats) - See attached photo

<strong> </strong><strong>KNOWLEDGE, SKILLS &amp; ABILITIES/CREDENTIALS: </strong>

- Creative and/or passionate about consumer marketing

-  Working knowledge of PhotoShop, Illustrator, InDesign, PowerPoint-- a must!

- Amazing social media and copy writing skills are a plus!

- Minimum 1-2 years of experience in related fields is ideal!

- Ability to work well under pressure and meet project deadlines

- Strong preference for individuals with (or pursuing) a BS/BA in Graphic Design, Marketing, Art, Photograpy or a related field

<strong>KEY PERSONALITY TRAITS:</strong>

- Exceptional communication skills

- Exceptional multitasking

- Passion for customer service and complete satisfaction

- Ability to work both independently and remotely

- Ability to take direction and complete assignments on deadline

*You don't need to be a student, but can get credit if you are**

* Interested applicants should send cover letters and resumes to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a> for consideration.[/vc_column_text][vc_column_text]
<h1><strong>Corporate and Community Relations  – Chicago Only</strong></h1>
We are looking to hire a part-time Corporate Relations to liaise with Chicago corporations and to  facilitate the implementation of projects related to commitment to community relations and being a valued neighbor in those communities that we serve. This position requires close coordination and communication with our Marketing Department.

<strong>Responsibilities:</strong>
<ul>
 	<li>Develop and implement an integrated community relations plan that aligns with the strategic priorities.</li>
 	<li>Work to enhance tgin’s image and reputation internally and externally through partnerships with local schools and cancer related organizations</li>
 	<li>Create and execute plan to develop relationships with key organizations in the communities tgin serves.</li>
 	<li>Manage and select sponsorships and event participation with community partners.</li>
</ul>
<strong>Requirements:</strong>
• Demonstrated leadership, interpersonal, written and oral communication, problem solving, coaching, organizational, and presentation skills.
• Demonstrated evidence of building client relationships and dealing with diverse clients.
• Computer proficiency in MS Office, Outlook, and Internet required.
• Demonstrated evidence of detail-orientation, customer service orientation, ability to work independently, and high levels of responsibility required.
• Ability to travel
• Bachelor’s degree required.

Part-Time

Please email resume, cover letter and hourly pay requirements to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>.[/vc_column_text][vc_column_text]
<h1><strong>Social Media Team Member (Instagram/Facebook) – Anywhere</strong></h1>
<strong>Responsibilities: </strong>
<ul>
 	<li>Post three times a week 6-8x a day to Instagram and Facebook</li>
 	<li>Measure, analyze and report on performance of social media posts deriving actionable insights to improve performance, conversion rates, and loyalty, and increase awareness and trial among core consumer target</li>
 	<li>Analyze market competition regularly.</li>
 	<li>Identify innovative marketing ideas, contests and promotions to engage with core audience</li>
</ul>
<strong>COMPENSATION: </strong>TBD, but negotiable based on experience.
<div class="gmail_default">To apply,</div>
<div class="gmail_default"><span style="color: #676767; font-family: 'Open Sans';">(1) Please review our IG page @tginatural to get a sense of our flow and style. </span></div>
<div class="gmail_default"><span style="color: #676767; font-family: 'Open Sans';">(2) After that, please create a fake IG page (without tgin in the name) and post 12 photos with captions. You should post pictures that you think we would post to @tginatural based on the look/feel/content of our brand.. </span><em>Note, with the exception of product photos, these should not be pictures we have posted before to our page. We are looking for someone who can immediately jump in and gets our style and flow, and your pictures should reflect that.</em> Applicants will be judged on the visual quality of their photos, the flow of their page, and their captions/hashtags.</div>
<div class="gmail_default"><span style="color: #676767; font-family: 'Open Sans';">(3) Email your resume and a link to you fake Instagram page to </span><a href="mailto:tgincareers@gmail.com" target="_blank" rel="noopener">tgincareers@gmail.com</a>"</div>
[/vc_column_text][vc_column_text]
<h1> <strong>tgin Copywriter - Freelance</strong></h1>
We’re seeking a creative wordsmith with a strong working knowledge of social-media marketing and a passion for the digital space. The Junior Copywriter will write, review and edit review-brand copy for our existing line and new product launches.

<strong>REQUIREMENTS</strong>

Responsibilities:
<ul>
 	<li>Work with account teams to develop creative copy for social media, including Facebook, Instagram, and Twitter.</li>
 	<li>Develop creative marketing copy for promotions, digital marketing campaigns, videos, and more</li>
 	<li>Actively seek out and help define new creative possibilities for client engagements and ensure consistency and adherence to corporate brand requirements</li>
 	<li>Excellent verbal and written communication skills</li>
 	<li>Ability to write creatively while managing a consistent workload</li>
 	<li>Must be detail-oriented and know how to solve a problem</li>
 	<li>A desire to work in an exciting and demanding start-up environment</li>
</ul>
<strong>COMPENSATION: </strong>TBD, but negotiable based on experience.

Please email resume, cover letter, 3 writing samples and salary requirements to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>[/vc_column_text][vc_column_text]
<h1><strong>Marketing Coordinator – Product Development - Chicago Only</strong></h1>
<h2><strong>Responsibilities:</strong></h2>
<ul>
 	<li>Identify innovation hunting grounds and new product opportunities (hair/skin/lifestyle)</li>
 	<li>Contribute to new product ideation including but not limited to concept, packaging, naming, formula, product launch plans and cost of goods sold</li>
 	<li>Draft new product consumer communications and retail sell-in tools</li>
 	<li>Develop and execute new product positioning statements and write new product concepts.</li>
 	<li>Prepare and circulate New Product Sheets, puts together mock-ups of products, fills out Web Sheets, and ensures all appropriate paper work for tgin products are submitted to the appropriate manager.</li>
 	<li>Assess new product innovation performance to identify key learnings that can be applied to future product launches</li>
</ul>
<strong>COMPENSATION: </strong>TBD, but negotiable based on experience.

Please email resume and cover letter to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>[/vc_column_text][vc_column_text]
<h1><strong>Digital Marketing Manager – Chicago Only</strong></h1>
<h2><strong>Responsibilities:</strong></h2>
<ul>
 	<li>Lead strategy and execution of digital, social, mobile, and PR with an emphasis on inbound marketing strategies to deliver brand engagement.</li>
 	<li>Create sales funnels and track conversions</li>
 	<li>Maintain and monitor project timelines and follow up with crossfunctional team members on an on-going basis</li>
 	<li>Generate customized analysis including online media, search engine campaigns, social metrics, mobile metrics, and web analytics that highlight campaign strengths and weaknesses and provide actionable insights on campaign performance</li>
 	<li>Own certain client accounts and ensure they are running optimally. This includes daily client interaction and service, supervision of a client’s content and advertising, as well as ensuring that work is produced on time and within budget</li>
 	<li>Tracks promotional budget for digitial marketing activity including expenditures versus income.</li>
 	<li>Measure, analyze and report on performance of all digital marketing &amp; PR campaigns deriving actionable insights to improve performance, conversion rates, and loyalty, and increase awareness and trial among core consumer target</li>
 	<li>Analyze market competition regularly including pulling together competitive samples, pricing, and researching selling copy to guide annual operations plans and ongoing business planning.</li>
</ul>
<strong>COMPENSATION: </strong>TBD, but negotiable based on experience.

Please email resume and cover letter to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>[/vc_column_text][vc_column_text]
<h1><strong>tgin Administrative Assistant - Operations</strong></h1>
<strong><u>Company Overview</u></strong>

Join the creative and energetic <strong>Thank God It’s Natural (tgin)</strong> Operations Team, and help us further enhance our strong brand. We are a small natural hair business looking to hire an Administrative Assistant to assist our Operations Team, which is primarily responsible for ordering the necessary components of our products and shipping and delivering orders to major retailers. As our Administrative Assistant, you must be meticulous and extremely detailed oriented, comfortable working in a fast pace environment and capable multi-tasking. Experience with Facebook Ads or social media is a major bonus/plus.

<strong><u>Responsibilities</u></strong><strong>:</strong>
<ul>
 	<li>Manage multiple e-mail accounts (admin account and CEO’s account)</li>
 	<li>Calendar/Schedule Management for CEO</li>
 	<li>Order supplies for production runs</li>
 	<li>Schedule deliveries for retailers using our logistics system</li>
 	<li>Update account payables</li>
 	<li>Send CEO an email of all major emails at the end of the day</li>
 	<li>Coordinate and plan events according to event checklist</li>
 	<li>Return Calls and Voicemails, Reminder Calls/Texts</li>
 	<li>Travel Planning (Manage Travel Preferences for Air, Hotel, Car, Vacations, Private Charter Flights, &amp; etc)</li>
 	<li>Event Planning, Prep, Coordination, &amp; Meal Planning</li>
 	<li>Notetaking on conference calls</li>
 	<li>Social Media Administration (Blogs/Postings, Facebook, Twitter, LinkedIn, YouTube, etc.) – We have a social media team, but there may be some minor posts that you are required to put up occasionally.</li>
 	<li>Editing &amp; Proofreading</li>
 	<li>On-line File Management</li>
</ul>
<strong><u>Education and Experience:</u></strong>
<ul>
 	<li>Strong Written and Oral Communication Skills</li>
 	<li>Extremely Organized</li>
 	<li>Acute multi-tasking skills</li>
 	<li>Flexible and able to Work in a Fast Paced Environment</li>
 	<li>Exceptional Time Management Skills</li>
 	<li>Analytical</li>
</ul>
<strong>COMPENSATION: </strong>TBD, but negotiable based on experience.

Please email resume,  cover letter and salary requirements to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>[/vc_column_text][vc_column_text]
<h1><strong>tgin Administrative Assistant - Marketing</strong></h1>
<strong><u>Company Overview</u></strong>

Join the creative and energetic <strong>Thank God It’s Natural (tgin)</strong> Marketing Team, and help us further enhance our strong brand. We are a small natural hair business looking to hire a Administrative Assistant to assist our Marketing Team with special projects as we expand our brand across several national retailers. As our Administrative Assistant, you must be meticulous and extremely detailed oriented, comfortable working in a fast pace environment and capable multi-tasking. Experience with Facebook Ads or social media is a major bonus/plus.
<strong><u>Sample Tasks</u>:</strong>
<ul>
 	<li>Work with graphic designer on updating images</li>
 	<li>Ordering banners from Kinkos</li>
 	<li>Ordering brochures, t-shirts and marketing collateral</li>
 	<li>Planning events according to our event checklist</li>
 	<li>Executing on social media campaigns (e.g. sending prizes to winners)</li>
 	<li>Maintaining a timeline and checklists of ongoing projects</li>
 	<li>Completing event applications</li>
 	<li>Making travel arrangements</li>
 	<li>Following up with team members, vendors, etc. for outstanding items</li>
 	<li>Running Facebook Ads</li>
 	<li>Working with marketing team to develop project plans, schedules, and assign responsibilities</li>
</ul>
<strong><u>Job Responsibilities:</u></strong>
<ul>
 	<li>Define and clarify project scope, plan, and schedule.</li>
 	<li>3+ years of experience producing or managing digital and/or experiential projects.</li>
 	<li>Comfort working within fast-paced timelines while managing multiple responsibilities.</li>
 	<li>Strategic thinking skills to identify risks or opportunities during the life of a project.</li>
 	<li>Experience working with clients, vendors and internal teams to ensure the delivery of outstanding projects on time and on budget.</li>
 	<li>Anticipates and overcomes bottlenecks and obstacles to completing projects.</li>
 	<li>Documents project progress and updates senior management regularly and as required by management.</li>
 	<li>Provides periodic reports on project status milestones and adjusts schedules accordingly.</li>
 	<li>Monitors design changes, specifications, and drawing releases and ensures changes are communicated and, where necessary, properly approved.</li>
 	<li>Research vendors and controls expenditures within limitations of project budget.</li>
 	<li>Works diligently to get projects that are off schedule back on schedule to the original due date of the project.  Directly accountable for raising the issue to higher level management that a project is off schedule.</li>
 	<li>Manage Travel Preferences for Air, Hotel, Car, Vacations, Private Charter Flights, &amp; etc.</li>
 	<li>Return Voicemails, Text messages and Emails in addition to Calendar management.</li>
 	<li>Develop policies and procedures to support the achievement of the project objectives.</li>
 	<li>Demonstrate a commitment to tgin core values.</li>
 	<li>Ad hoc responsibilities as needed.</li>
</ul>
<strong><u>Education and Experience:</u></strong>
<ul>
 	<li>Strong Written and Oral Communication Skills</li>
 	<li>Extremely Organized</li>
 	<li>Acute multi-tasking skills</li>
 	<li>Flexible and able to Work in a Fast Paced Environment</li>
 	<li>Exceptional Time Management Skills</li>
 	<li>Analytical</li>
</ul>
<strong>COMPENSATION: </strong>TBD, but negotiable based on experience.

Please email resume,  cover letter and salary requirements to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>[/vc_column_text][vc_column_text]
<h1><strong>Marketing Sponsorship Intern - Chicago</strong></h1>
<strong><u>Position Overview</u></strong>

The Marketing Sponsorship Intern will be responsible for the daily checking of the sponsorship email account, reading the sponsorship requests and responding appropriately. The sponsorship intern will also be responsible for communicating requests to the marketing team as well as the charting of all requests. Experience with Microsoft excel is a plus.

The Marketing Sponsorship Intern will report to the Brand Manager but will also have the opportunity to work alongside other tgin marketing team members as well as communicating with many of tgin’s partners such as beauty influencers, bloggers, editors and philanthropists.

<strong><u>Job Responsibilities</u></strong>

Primary duties include but are not limited to the following:
<ul>
 	<li>Email account management through daily checking and organization</li>
 	<li>Reading, evaluating and responding to sponsorship requests</li>
 	<li>Daily communication about requests to the Brand Manager</li>
 	<li>Chart all requests on the sponsorship excel drive sheet</li>
 	<li>Place sponsorship request product orders using our online 3dcart system</li>
 	<li>Attend marketing team conference calls (as needed)</li>
</ul>
<strong><u>Education and Experience:</u></strong>
<ul>
 	<li>Must be open to taking direction and able to adapt to a fast paced environment.</li>
 	<li>At least 1-year customer service experience is required</li>
 	<li>Able to work independently and make decisions on a case by case basis</li>
 	<li>Extremely strong work ethic and attention to detail</li>
 	<li>Experience working with philanthropic organizations</li>
 	<li>Strong people skills and team-player mentality</li>
 	<li>Educational requirements:  Marketing, communications, sales, writing/journalism experience is a plus though not a requirement.</li>
 	<li>Current college students who fit the above criteria are also encouraged to apply.</li>
 	<li>Marketing experience is a plus, but not required</li>
</ul>
<strong>Compensation: TBD</strong>

Please email resume,  cover letter and salary requirements to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>[/vc_column_text][vc_column_text]
<h1><strong>Marketing Intern - Anywhere</strong></h1>
<strong><u>Position Overview</u></strong>

<strong>Thank God It’s Natural (tgin) </strong>is seeking Marketing Intern to join our team. This position will be responsible for leading the strategy, sale, and execution of day-to-day influencer marketing activities on behalf of tgin. This position, reporting to the CEO, will be an integral part of managing our brand partnership and influencer relationships.   The successful candidate will be a self-starter with a demonstrated ability to manage event planning, program logistics, juggle multiple projects and possess a solid client service and influencer marketing track record. Job will require 10-15 hour/wk commitment.

<strong><u>Responsibilities</u></strong>
<ul>
 	<li>Manage influencer marketing efforts – Identify, recruit and manage influencer relationships.</li>
 	<li>Negotiate and execute paid influencer programs on behalf of tgin, including news items, product reviews, content exchanges, product placement, storytelling, user-generated content, social engagement, et al.</li>
 	<li>Develop inventive promotional and event marketing opportunities that effectively expands our audience base and garners consumer/industry buzz and attention.</li>
 	<li>Evaluate, structure, &amp; negotiate inbound brand partnerships</li>
 	<li>Manage campaign operations including scheduling, billing, &amp; execution</li>
 	<li>Submit monthly report to CEO of influencer campaigns and how they performed</li>
 	<li>Manage campaigns within monthly budget.</li>
 	<li>Work closely with other marketing, PR and production team members to ensure proper integration into marketing strategies or tactics.</li>
 	<li>Measure overall value/effectiveness of promotional campaigns, report and course correct as needed</li>
 	<li>Research and develop new methods to leverage influencers to achieve overall client objectives and goals.</li>
 	<li>Approve/Decline event sponsorship requests</li>
 	<li>Monitor trends in digital industry and write thought-leading content.</li>
 	<li>Understand and compliance with proper FTC guidelines for brand activities</li>
</ul>
<strong> </strong><strong><u>Education and Experience</u></strong>
<ul>
 	<li>Bachelor’s Degree in Journalism, English, Communications, Marketing or related field preferred.</li>
 	<li>Experience with social media platforms, including, but not limited to Facebook, Instagram, Pinterest, Twitter, YouTube, Tumblr, Google+ and LinkedIn</li>
 	<li>Excellent writing and editing skills required.</li>
</ul>
<strong>Compensation: TBD</strong>

Please email resume,  cover letter and salary requirements to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>[/vc_column_text][vc_column_text]
<h1><strong>Shipping and Receiving Manager — Chicago Only</strong></h1>
We are looking for individual who is a self-starter and has strong computer skills to head up our shipping and receiving department.
<h2><strong>Responsibilities</strong><b>:</b></h2>
<ul>
 	<li>Hours –  9 – 5 CST</li>
 	<li>Must have strong computer skills and be able to use Word, Excel, Power Point, email and social media</li>
 	<li>Operate the shopping cart software to retrieve and prepare orders</li>
 	<li>Package and ship daily orders via USPS</li>
 	<li>Order shipping supplies from USPS &amp; Uline while staying within a budget</li>
 	<li>Verify incoming shipments from UPS &amp; Fed Ex, and other various shipping companies</li>
 	<li>Communicate company policies and functions of products to customers</li>
 	<li>Provide services; process returns, exchanges, adjustments and payments</li>
 	<li>Train new employees on tgin shipping procedures</li>
 	<li>Create, package, and ship domestic/international wholesale orders</li>
 	<li>Schedule pickup times to ship daily orders (USPS) and wholesale orders (UPS)</li>
 	<li>Maintain/count warehouse inventory on a daily/monthly basis respectively</li>
 	<li>Alert Production Manager when stock is low for any product</li>
 	<li>Available to help bottle/label/make products whenever needed</li>
 	<li>Perform a daily clean of the shipping area and office to ensure organization and order</li>
 	<li>Understand the shelf life of tgin’s natural body and skin care products.</li>
 	<li>Assist Production Team with bottling and labeling on as needed basis</li>
 	<li>Assist with general office duties (e.g. contacting the management office about building and maintenance issues, ordering supplies, taking inventory, etc.)</li>
 	<li><strong>Must be able to pass a background check</strong></li>
</ul>
Interested candidates should email their resume and cover letter to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a> Subject: Shipping Department

[/vc_column_text][vc_column_text]
<h1><strong>Delivery Driver — Chicago Only</strong></h1>
tgin is currently seeking to hire a delivery driver who can work 2 days a week this summer in the mornings (7am – 1pm). Driver will be responsible for dropping off product at various Mariano’s stores throughout the city and suburb and completing an inventory report.
<h2><strong>Requirements</strong>:</h2>
<ul>
 	<li>Extreme attention to detail</li>
 	<li>Customer-friendly personality</li>
 	<li>Strong computer skills</li>
 	<li>Your own lap top</li>
 	<li>The ability to lift in excess of 20 lbs</li>
</ul>
Qualified applicants must have a valid driver’s license issued in the state of Illinois, their own vehicle and proof of insurance. Delivery Drivers are expected to comply with tgin  appearance guidelines and wear the company-provided uniform.  Applicants must be able to pass a criminal background check

Please email resume, cover letter and three references to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>

Pay: TBD + Mileage Reimbursement

[/vc_column_text][vc_column_text]
<h1><strong>tgin Amazon.com Store Manager Wanted — Chicago Only</strong></h1>
<p dir="ltr">We are looking for an organized and responsible individual that can help us fulfill orders we receive online from Amazon.com.   Experience selling items on Amazon or ebay is a plus. Bilingual candidates strongly encouraged to apply.</p>

<h2><strong>Job Description:</strong><strong>
<img class="size-full wp-image-28483 alignright" src="https://thankgodimnatural.com/tgin/wp-content/uploads/2016/06/Store-clerk-female1-300x300-1.jpg" alt="Store-clerk-female1-300x300" width="300" height="300" /></strong></h2>
<ul>
 	<li dir="ltr">Mail out packages five times a week to Amazon.com customers</li>
 	<li dir="ltr">Prepare weekly inventory report</li>
 	<li dir="ltr">Respond to customer service issues within 24 hours (e.g. I received the wrong package, what is my tracking #, letting people know items are on back order, etc.)</li>
 	<li dir="ltr">Update Amazon.com website to remove out of stock items.</li>
 	<li dir="ltr">Take customer orders over the phone when they encounter difficulties with placing an order online</li>
 	<li dir="ltr">Order supplies as needed (as needed)</li>
 	<li dir="ltr">Keep email account organized</li>
</ul>
<h2><strong>Requirements:</strong></h2>
<ul>
 	<li dir="ltr">Cell Phone</li>
 	<li>10-15 hour week time commitment</li>
 	<li>Strong Computer Skills/Ability to Use an Ipad</li>
 	<li>High School Diploma</li>
 	<li>Bilingual (not required, but a plus)<strong> </strong></li>
 	<li><strong>Pay:</strong> TBD</li>
</ul>
Interested applicants should send their resume, cover letter, and three references to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com.</a>

[/vc_column_text][vc_column_text]
<h1><strong>tgin Brand Ambassadors Wanted for Events + In Store Demos  - Chicago Only</strong></h1>
<div>Our events and promotion team is looking for young, energetic, health conscious women to promote our new tgin Moist Collection for natural hair. A background in retail/sales and a familiarity with natural hair/body products is a plus.</div>
<div></div>
<div>
<h2><strong>Job Description:</strong><strong> </strong></h2>
–          Handing Out Samples
–          Selling products and apparel
–          Promoting the Brand at local events (Shecky’s Girls Night Out, African Fest, Taste of Randolph, etc.)
–          Talking about health and the benefits of using natural and organic products
–          Event setup/break down
<h2><strong>Ideal Candidate Will Possess:</strong><strong>        </strong></h2>
–  Excellent leadership and communication skills.
–  Strong attention to customer service issues.
–  Prior merchandise planning responsibilities preferred.
–  Detail oriented with the ability to plan, organize and execute corporate goals.
–   Honest
–   Can work in a fast paced environment.
–   Hours: As Needed for Events

Interested applicants should send their resume, cover letter, and three references to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>.  Subject of email should be: TGIN Brand Ambassador.

</div>
[/vc_column_text][vc_column_text]
<div class="header">
<div class="logo">
<h1>tgin Hair Stylists to do Platform Work - Chicago Only</h1>
We are looking for experienced natural hair stylists to do platform work demonstrating how our products work at events. Interested applicants should send their<strong> </strong>resume, cover letter, <strong>hourly rate</strong> and sample of their work to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>. Subject of email should be: TGIN Hair Stylist

</div>
</div>
[/vc_column_text][vc_column_text]
<h1>Wholesale Account Manager – Filled</h1>
Tgin is a manufacturer of natural hair and skin care products primarily targeting women of color. We are looking to for someone, who can assist us in reaching out to/tracking stores that contact us about carrying our products and take detailed notes. A background in sales is a plus.

Hours: 10-15/hrs per week.
<h2><strong>Responsibilities:</strong></h2>
<ul>
 	<li>Contact prospects/leads via telephone and email with end result being an appointment that is then turned over to an outside 435 Digital SMB rep</li>
 	<li>Initiate a conversation with the decision maker to introduce our products and services,</li>
 	<li>Address client issues and sales activity with prospective and existing clients; track and monitor client satisfaction issues and work with Support Managers to resolve major problems.</li>
 	<li>Produce invoices and develop quotes for potential clients</li>
 	<li>Conduct competitive research and maintain knowledge of competitive products.</li>
 	<li>Document , track and monitor all leads and activities in CRM</li>
 	<li>Nurture longer term prospects</li>
 	<li>Follow up with prospects who responded to marketing programs and campaigns</li>
 	<li>Assist with list building activities</li>
</ul>
<h2><strong>Experience:</strong><strong> </strong></h2>
<ul>
 	<li>1-2 years experience in marketing / sales preferred, but make exceptions for the right candidate</li>
 	<li>Experience in customer service or other people-oriented fields desired</li>
 	<li>Exceptional organizational and project management skills</li>
 	<li>Exceptional communication skills</li>
 	<li>Ability to work independently and contribute in a team environment</li>
 	<li>Excellent writing skills.</li>
</ul>
Interested applicants should send their resume, cover letter, and three references to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>.  Subject of email should be: TGIN Wholesale Account Manager.[/vc_column_text][vc_column_text]
<div class="header">
<div class="logo">
<h1>tgin Customer Service Representative -Anywhere - FILLED</h1>
We are looking for a smart, articulate individual to responsible to respond to customer service emails and voicemails. Strong background in customer service is a plus.
<h2><strong>Job Description:</strong></h2>
–          Respond to customer emails and voicemails 2x a day (am and pm).
–          Respond to customer Facebook and Instagram postings daily 2x a day (am and pm).
–          Identify any major customer service issues/trends.
–          Send daily summary email of issues requiring attention to Online Store Manager and management.
–          Hours: 1 hour/day or 8 hours/week
Interested applicants should send their <strong>r</strong>esume, cover letter, and three references to <a href="mailto:thankgodimnatural@gmail.com" target="_blank" rel="noopener">thankgodimnatural@gmail.com</a>. Subject of email should be: TGIN Customer Service Representative

</div>
</div>
[/vc_column_text][vc_column_text]
<h1>tgin Production Team Leader Wanted — Filled</h1>
We are looking for an individual to work in our small hair care factory in the Production Department. The ideal candidate has the ability to multi-task and also focus on one task at a time (e.g. filling 200 of a product in 8 hours).  Must be efficient and be able to generate a certain amount of output in a certain amount of time. Warehouse and management experience is a plus.  Disclaimer: Several members of our team are OCD, which means they like for things to be done a certain way and are very organized. Sample responsibilities are below:

• Bottle/label/make conditioners, honey miracle, shea butter, argan oils, sample packs, etc.
products whenever needed
• Train new production team members on production policies
• Maintain/Count warehouse inventory every Monday
• Alert Operations Team/Office Manager when supplies of any product is low or too high
• Submit a production schedule to Operations Team every Friday
• Meet with Operations Team every Monday to agree on production goals for the week
• Perform a daily clean of the production area and office to ensure organization and order
• Understand the shelf life of tgin’s natural hair and skin care products.
• Stock shelves using the FIFO method

Hours: 20 hours/week

Interested applicants should send their resume, cover letter, and three references to <a href="mailto:tgincareers@gmail.com">tgincareers@gmail.com</a>.  Subject of email should be: TGIN Production Team. Spanish speakers/bilingual candidates are strongly encouraged to apply.

[/vc_column_text][/vc_column][/vc_row]";s:10:"post_title";s:11:"Our Careers";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:11:"our-careers";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-09-12 11:03:12";s:17:"post_modified_gmt";s:19:"2018-09-12 11:03:12";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:48:"http://thankgodimnatural.com/tgin/?page_id=28480";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}