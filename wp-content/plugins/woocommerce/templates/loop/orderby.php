<?php
/**
 * Show options for ordering
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/orderby.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<div class="col-md-3">
	<div class="sort-by">
<form class="woocommerce-ordering">
<select name="orderby" id="dynamic_select" class="orderby">
  <option value="?">Browse by Price</option>
  <option value="?min_price=0&max_price=24.99" <?php if($_GET['min_price']==0 && $_GET['max_price']==24.99) { echo 'selected'; } ?>>$0 - $24.99</option>
  <option value="?min_price=25&max_price=49.55" <?php if($_GET['min_price']==25 && $_GET['max_price']==49.55) { echo 'selected'; } ?>>$25 - $49.55</option>
  <option value="?min_price=50" <?php if($_GET['max_price']==50) { echo 'selected'; } ?>>Over $50</option>
</select>
</form>

<script>
    $(function(){
      // bind change event to select
      $('#dynamic_select').on('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
    });
</script>
</div>
</div>
<div class="col-md-2">
	<div class="sort-by">
<form class="woocommerce-ordering" method="get">
	<select name="orderby" class="orderby">
		<?php foreach ( $catalog_orderby_options as $id => $name ) : ?>
			<option value="<?php echo esc_attr( $id ); ?>" <?php selected( $orderby, $id ); ?>><?php echo esc_html( $name ); ?></option>
		<?php endforeach; ?>
	</select>
	<input type="hidden" name="paged" value="1" />
	<?php wc_query_string_form_fields( null, array( 'orderby', 'submit', 'paged', 'product-page' ) ); ?>
</form>
</div>
</div>
