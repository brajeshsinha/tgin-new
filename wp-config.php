<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'import2');

/** MySQL database username */
define('DB_USER', 'import2_admin');

/** MySQL database password */
define('DB_PASSWORD', '-b,wTLbTEDD}');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '*KsYB(WX}A5ef>;$G#[-:~:bUUG=ZGN.yXL]vQ_4SqaS]=dQ_Pv`*BHg1CAhk7#A');
define('SECURE_AUTH_KEY',  's,/&zUmxwX|RU !g{q:D!yFzqa,!@a!UAVeunF!H%,zDCw+ 88Z{,m[A2t_x*Sp;');
define('LOGGED_IN_KEY',    'xiu-a,2x2nhU_=&>ld*E!9]{a]G]({NJog+vde*?U0!e-(/{aLQm}+._RE/S)sfM');
define('NONCE_KEY',        '@2_@~J^nM}i4E1=Ind:Gf#>:12Z4n?_PWSWyEvt:27JZ wg0b>u {GuGYf?)$)$S');
define('AUTH_SALT',        '0-N%g)Me8<c&V^{!T&d4jIAgROKCP*[zL:5cm)Ot&pW~L4g]}grhC)9I2u[I;.|N');
define('SECURE_AUTH_SALT', 'deU,DuY7ZzRw9MR:Z`,i^u8^Nm{Oo%#qS_siiT]3cjNm)~-mAEWv0Hk#r+xDV[Bo');
define('LOGGED_IN_SALT',   'dg6-gva|bD!4*7`Yl$uvVcG(y3#seIQBk$^>U~6d~#A|{T7m71FNZ_x U,jsaR%A');
define('NONCE_SALT',       '8e(7IH|c3,o=E)7wn[(UMM@DBan=bqi^p^n}tmf[YKJqAhC4N/r6cdIEb-.9Y5,9');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
